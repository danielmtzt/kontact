<?php
require_once("s/db.php");
if(isset($_POST['nombre'])){
$intUsuario=$_SESSION['IDUS'];
if(isset($_POST['nombre'])){
$txtEmpresa=$mysqli->real_escape_string($_POST['nombre']);
}else{$txtEmpresa="";}
if(isset($_POST['calle'])){
$txtCalle=$mysqli->real_escape_string($_POST['calle']);
}else{$txtCalle="";}
if(isset($_POST['noext'])){
$txtNoExt=$mysqli->real_escape_string($_POST['noext']);
}else{$txtNoExt="";}
if(isset($_POST['noint'])){
$txtNoInt=$mysqli->real_escape_string($_POST['noint']);
}else{$txtNoInt="";}
if(isset($_POST['colonia'])){
$txtColonia=$mysqli->real_escape_string($_POST['colonia']);
}else{$txtColonia="";}
if(isset($_POST['municipio'])){
$txtMunicipio=$mysqli->real_escape_string($_POST['municipio']);
}else{$txtMunicipio="";}
if(isset($_POST['estado'])){
$txtEstado=$mysqli->real_escape_string($_POST['estado']);
}else{$txtEstado="";}
if(isset($_POST['cp'])){
$txtCP=$mysqli->real_escape_string($_POST['cp']);
}else{$txtCP="";}
if(isset($_POST['latitud'])){
$dblLat=$mysqli->real_escape_string($_POST['latitud']);
}else{$dblLat="";}
if(isset($_POST['longitud'])){
$dblLong=$mysqli->real_escape_string($_POST['longitud']);
}else{$dblLong="";}
if(isset($_POST['fbid'])){
$FBID=$mysqli->real_escape_string($_POST['fbid']);
}else{$FBID="";}
if(isset($_POST['twid'])){
$TWID=$mysqli->real_escape_string($_POST['twid']);
}else{$TWID="";}
if(isset($_POST['tel'])){
$txtTelefono=$mysqli->real_escape_string($_POST['tel']);
}else{$txtTelefono="";}
if(isset($_POST['email'])){
$txtEmail=$mysqli->real_escape_string($_POST['email']);
}else{$txtEmail="";}
if(isset($_POST['web'])){
$txtWebsite=$mysqli->real_escape_string($_POST['web']);
}else{$txtWebsite="";}
$datetime=getUTC();
$txtBuscador=$txtEmpresa;
$query=sprintf("INSERT INTO tblempresas (txtEmpresa, txtCalle, txtNoExt, txtNoInt, txtColonia, txtMunicipio, intEstado, txtCP, dblLat, dblLong, FBID, TWID, txtTelefono, txtEmail, datetime, txtBuscador, intUsuario,txtWebsite) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')",$txtEmpresa,$txtCalle,$txtNoExt,$txtNoInt,$txtColonia,$txtMunicipio,$txtEstado,$txtCP,$dblLat,$dblLong,$FBID,$TWID,$txtTelefono,$txtEmail,$datetime,$txtBuscador,$intUsuario,$txtWebsite);
$mysqli->query($query);
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<title>Agregar Empresa - Buscador RT</title>
	<?php require_once("meta.php"); ?>
	<?php require_once("css.php"); ?>
	<?php require_once("js.php"); ?>
</head>
<body>
	<div id="container">
		<?php $ht="Agregar Empresa"; require_once("header.php"); ?>
		<form id="register" method="POST">
			<input name="nombre" type="text" placeholder="Nombre" required>
			<select name="categoria">
				<option value="24">Categoría...</option>
				<?php
				$query=sprintf("SELECT * FROM tblcategorias ORDER BY txtCategoria ASC");
				$categorias=$mysqli->query($query);
				$c=$categorias->fetch_assoc();
				do{
					echo '<option value="'.$c['intCategoria'].'">'.$c['txtCategoria'].'</option>';
				}while($c=$categorias->fetch_assoc());
				?>
			</select>
			<select name="estado">
				<option value="24">San Luis Potosí</option>
			</select>
			<input name="calle" type="text" placeholder="Calle">
			<input name="noext" type="text" placeholder="No. Exterior">
			<input name="noint" type="text" placeholder="No. Interior">
			<input name="colonia" type="text" placeholder="Colonia">
			<input name="municipio" type="text" placeholder="Municipio">
			<input name="cp" type="text" placeholder="Código Postal">
			<input name="web" type="text" placeholder="Sitio Web">
			<input name="fbid" type="text" placeholder="Facebook ID">
			<input name="twid" type="text" placeholder="Twitter ID">
			<input name="tel" type="text" placeholder="Teléfonos">
			<input name="email" type="email" placeholder="E-mail">
			<input type="hidden" name="latitud" id="latitud">
			<input type="hidden" name="longitud" id="longitud"><br>
		
			<button class="btn" id="setcurrent"><i class="fa fa-map-marker"></i> Ubicación Actual</button>
			<div id="map" style="width:100%;height:300px;max-width:500px;margin: 10px auto;border:1px solid #bbb;"></div>
			<input type="submit" value="+ Agregar Empresa" class="btn" style="width:auto;">
		</form>
	</div>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAl4aQuUYYqyIHtkOOYOHwV3LKcJBsIDrM&callback=initMap"
        async defer></script>
    <script type="text/javascript">
    var map; var marker;
    function initMap() {
		map = new google.maps.Map(document.getElementById('map'), {
		center: {lat:22.152131, lng:-100.977923},
		zoom: 16
		});
		marker = new google.maps.Marker({
		map: map,
		draggable: true
		});
		map.addListener('click', function(e) {
			console.log(e);
			$('#latitud').val(e.latLng.lat());
			$('#longitud').val(e.latLng.lng());
			marker.setPosition(e.latLng);
		});
		google.maps.event.addListener(marker, 'dragend', function (event) {
			$('#latitud').val(this.getPosition().lat());
			$('#longitud').val(this.getPosition().lng());
		});
	}
	$(document).ready(function() {
		$('#setcurrent').on("click",function(e){
			e.stopPropagation();
			e.preventDefault();
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function(position) {
					var pos = {
					lat: position.coords.latitude,
					lng: position.coords.longitude
					};
					$('#latitud').val(position.coords.latitude);
					$('#longitud').val(position.coords.longitude);
					map.setCenter(pos);
					marker.setPosition(pos);
				}, function(e) {
				//handleLocationError(true, infoWindow, map.getCenter());
				});
			}
		});
	});
    </script>
</body>
</html>
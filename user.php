<?php
require_once("s/db.php");
$intUsuario=$_SESSION['IDUS'];
$query=sprintf("SELECT * FROM tblusuarios WHERE intUsuario=%s LIMIT 1",$intUsuario);
$i=$mysqli->query($query);
$u=$i->fetch_assoc();
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<title><?php echo $u['txtUsuario']; ?> - Kontact</title>
	<?php require_once("meta.php"); ?>
	<?php require_once("css.php"); ?>
	<?php require_once("js.php"); ?>
	<script type="text/javascript">
	$(document).ready(function() {
		$('.search-results').each(function(index, el) {
			var data=$(this).data('search');
			var r='';
			for (var i = 0; i < data.length; i++) {
				r+=sre(data[i]);
			};
			$(this).html(r).show();
			setFavoritos();
		});
		
	});
	</script>
</head>
<body>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.7&appId=358644980900253";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<div id="container">
		<?php $ht=$u['txtUsuario'];require_once("header.php"); ?>
		<?php
		$r=array();
		$query=sprintf("SELECT txtEmpresa,txtCalle,txtNoExt,txtNoInt,txtColonia,txtTelefono,tblempresas.intEmpresa,tblempresas.txtSEO,hasProfile,FBID FROM tblfavoritos LEFT JOIN tblempresas ON tblempresas.intEmpresa=tblfavoritos.intEmpresa WHERE tblfavoritos.intUsuario=%s",$intUsuario);
		$i=$mysqli->query($query);
		if($i->num_rows>0){
		$e=$i->fetch_row();
		do{
			array_push($r, $e);
		}while($e=$i->fetch_row());
		}
		$r2=array();
		$query=sprintf("SELECT txtEmpresa,txtCalle,txtNoExt,txtNoInt,txtColonia,txtTelefono,tblempresas.intEmpresa,tblempresas.txtSEO,hasProfile,FBID, MAX(intRegistro) FROM tblviews LEFT JOIN tblempresas ON tblempresas.intEmpresa=tblviews.intEmpresa WHERE tblviews.intUsuario=%s GROUP BY intEmpresa ORDER BY MAX(intRegistro) DESC LIMIT 10",$intUsuario);
		$i=$mysqli->query($query);
		if($i->num_rows>0){
		$e=$i->fetch_row();
		do{
			array_push($r2, $e);
		}while($e=$i->fetch_row());
		}
		?>
		<div id="profile-header">
			<h3><i class="kon-star"></i> Favoritos</h3>
			<?php if(count($r)>0){ ?>
			<ul class="search-results" data-search='<?php echo json_encode($r); ?>'></ul>
			<?php }else{ ?>
			<div class="tac">Agrega empresas a tus favoritos dando clic en <i class="kon-star"></i>.</div>
			<?php } ?>
			<h3><i class="kon-check-circle"></i> Visitados</h3>
			<?php if(count($r2)>0){ ?>
			<ul class="search-results" data-search='<?php echo json_encode($r2); ?>'></ul>
			<?php }else{ ?>
			<div class="tac">Aquí podrás ver las empresas que has visitado en la página.</div>
			<?php } ?>
		</div>
	</div>
</body>
</html>
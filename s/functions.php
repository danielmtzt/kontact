<?php
function view($intEmpresa){
	global $mysqli;
	if(isset($_SESSION['IDUS'])){
		$intUsuario=$_SESSION['IDUS'];
	}else{
		$intUsuario=0;
	}
	$ip=$_SERVER['REMOTE_ADDR'];
	$query=sprintf("INSERT INTO tblviews (intEmpresa,intUsuario,txtIP,datetime) VALUES (%s,%s,'%s','%s')",$intEmpresa,$intUsuario,$ip,getUTC());
	$mysqli->query($query);
}
function empresa($intEmpresa){
	global $mysqli;
	$query=sprintf("SELECT * FROM tblempresas WHERE intEmpresa=%s",$intEmpresa);
	$i=$mysqli->query($query);
	$r=$i->fetch_assoc();
	return $r;
}
function strEstado($e){
	if($e==24){$e="San Luis Potosí";}
	return $e;
}
function getDireccion($e){
	$d="";
	if($e['txtCalle']!=""){
		$d.=$e['txtCalle']." ";
	}
	if($e['txtNoExt']!=""){
		$d.="#".$e['txtNoExt'];
	}
	if($e['txtNoInt']!=""){
		$d.="-".$e['txtNoInt'];
	}
	$d.=" ";
	if($e['txtColonia']!=""){
		$d.="Col. ".$e['txtColonia']." ";
	}
	if($e['txtMunicipio']!=""){
		$d.=$e['txtMunicipio'].", ";
	}
	if(trim($d)!=""){
		$d.=strEstado($e['intEstado']);
	}
	return trim($d);
}
function getUTC(){
	$dt=new DateTime(NULL,new DateTimeZone("UTC"));
	return $dt->format("Y-m-d H:i:s");
}
function isFav($intEmpresa){
	global $mysqli;
	$r=false;
	if(isset($_SESSION['IDUS'])){
	$intUsuario=$_SESSION['IDUS'];
	$query=sprintf("SELECT * FROM tblfavoritos WHERE intUsuario=%s AND intEmpresa=%s",$intUsuario,$intEmpresa);
	$check=$mysqli->query($query);
	if($check->num_rows==1){$r=true;}
	}
	return $r;
}
function seo($text){
	$text = trim(htmlentities($text, ENT_QUOTES, 'UTF-8'));
	$text = strtolower($text);
	$patron = array (
	'/[\`\.\¿\?\-\!\¡:, ]+/' => '-',
	'/&amp;/' => '',
	'/&iquest;/'=>'',
	'/&iexcl;/'=>'',
	'/&agrave;/' => 'a',
	'/&egrave;/' => 'e',
	'/&igrave;/' => 'i',
	'/&ograve;/' => 'o',
	'/&ugrave;/' => 'u',
	'/&aacute;/' => 'a',
	'/&eacute;/' => 'e',
	'/&iacute;/' => 'i',
	'/&oacute;/' => 'o',
	'/&uacute;/' => 'u',
	'/&acirc;/' => 'a',
	'/&ecirc;/' => 'e',
	'/&icirc;/' => 'i',
	'/&ocirc;/' => 'o',
	'/&ucirc;/' => 'u',
	'/&atilde;/' => 'a',
	'/&etilde;/' => 'e',
	'/&itilde;/' => 'i',
	'/&otilde;/' => 'o',
	'/&utilde;/' => 'u',
	'/&auml;/' => 'a',
	'/&euml;/' => 'e',
	'/&iuml;/' => 'i',
	'/&ouml;/' => 'o',
	'/&uuml;/' => 'u',
	'/&auml;/' => 'a',
	'/&euml;/' => 'e',
	'/&iuml;/' => 'i',
	'/&ouml;/' => 'o',
	'/&uuml;/' => 'u',
	'/&aring;/' => 'a',
	'/&ntilde;/' => 'n',
	'/&ordf;/' => 'a',
	'/ordf;/' => 'a',
	'/impresion/'=>'imp',
	'/variante/'=>'var',
	'/&quot;/' => '',
	'/&ordm;/'=>'o',
	'/&deg;/'=>'',
	'/&acute;/'=>'-');
	$text = preg_replace(array_keys($patron),array_values($patron),$text);
	$simbolos = array("(", ")", "p/","c/","/","#","\\","\"","+","++","---","----","&039;");
	$text=str_replace($simbolos,"",$text);
	$simbolos = array("-y-", "-e-","-de-", "-la-", "-con-","-para-","-en-","-o-","--");
	$text=str_replace($simbolos,"-",$text);
	$text=trim($text,"-");
	return $text;
}
?>
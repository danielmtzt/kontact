<?php
require_once("db.php");
$intUsuario=$_SESSION['IDUS'];
$intEmpresa=$mysqli->real_escape_string($_POST['empresa']);
$query=sprintf("SELECT * FROM tblfavoritos WHERE intUsuario=%s AND intEmpresa=%s LIMIT 1",$intUsuario,$intEmpresa);
$check=$mysqli->query($query);
if($check->num_rows==0){
	$dt=new DateTime(NULL,new DateTimeZone("UTC"));
	$query=sprintf("INSERT INTO tblfavoritos (intUsuario,intEmpresa,datetime) VALUES (%s,%s,'%s')",$intUsuario,$intEmpresa,$dt->format("Y-m-d H:i:s"));
	$mysqli->query($query);
}else{
	$r=$check->fetch_assoc();
	$query=sprintf("DELETE FROM tblfavoritos WHERE intRegistro=%s",$r['intRegistro']);
	$mysqli->query($query);
}
?>
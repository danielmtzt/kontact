<?php
require_once("s/db.php");
$intEmpresa=$_GET['e'];

if(isset($_POST['edit'])){
	if(isset($_POST['nombre'])){
	$txtEmpresa=$mysqli->real_escape_string($_POST['nombre']);
	}else{$txtEmpresa="";}
	if(isset($_POST['calle'])){
	$txtCalle=$mysqli->real_escape_string($_POST['calle']);
	}else{$txtCalle="";}
	if(isset($_POST['noext'])){
	$txtNoExt=$mysqli->real_escape_string($_POST['noext']);
	}else{$txtNoExt="";}
	if(isset($_POST['noint'])){
	$txtNoInt=$mysqli->real_escape_string($_POST['noint']);
	}else{$txtNoInt="";}
	if(isset($_POST['colonia'])){
	$txtColonia=$mysqli->real_escape_string($_POST['colonia']);
	}else{$txtColonia="";}
	if(isset($_POST['municipio'])){
	$txtMunicipio=$mysqli->real_escape_string($_POST['municipio']);
	}else{$txtMunicipio="";}
	if(isset($_POST['estado'])){
	$txtEstado=$mysqli->real_escape_string($_POST['estado']);
	}else{$txtEstado="";}
	if(isset($_POST['cp'])){
	$txtCP=$mysqli->real_escape_string($_POST['cp']);
	}else{$txtCP="";}
	if(isset($_POST['latitud'])){
	$dblLat=$mysqli->real_escape_string($_POST['latitud']);
	}else{$dblLat="";}
	if(isset($_POST['longitud'])){
	$dblLong=$mysqli->real_escape_string($_POST['longitud']);
	}else{$dblLong="";}
	if(isset($_POST['fbid'])){
	$FBID=$mysqli->real_escape_string($_POST['fbid']);
	}else{$FBID="";}
	if(isset($_POST['twid'])){
	$TWID=$mysqli->real_escape_string($_POST['twid']);
	}else{$TWID="";}
	if(isset($_POST['tel'])){
	$txtTelefono=$mysqli->real_escape_string($_POST['tel']);
	}else{$txtTelefono="";}
	if(isset($_POST['email'])){
	$txtEmail=$mysqli->real_escape_string($_POST['email']);
	}else{$txtEmail="";}
	if(isset($_POST['web'])){
	$txtWebsite=$mysqli->real_escape_string($_POST['web']);
	}else{$txtWebsite="";}
	if(isset($_POST['descripcion'])){
	$txtDescripcion=$mysqli->real_escape_string($_POST['descripcion']);
	}else{$txtDescripcion="";}
	if(isset($_POST['categoria'])){
	$intCategoria=$mysqli->real_escape_string($_POST['categoria']);
	}else{$intCategoria="";}
	$datetime=getUTC();
	$txtBuscador=$txtEmpresa;
	$query=sprintf("UPDATE tblempresas SET txtEmpresa='%s', txtCalle='%s', txtNoExt='%s', txtNoInt='%s', txtColonia='%s', txtMunicipio='%s', intEstado='%s', txtCP='%s', dblLat='%s', dblLong='%s', FBID='%s', TWID='%s', txtTelefono='%s', txtEmail='%s', datetime='%s', txtBuscador='%s',txtWebsite='%s',txtDescripcion='%s',intCategoria='%s' WHERE intEmpresa=%s",$txtEmpresa,$txtCalle,$txtNoExt,$txtNoInt,$txtColonia,$txtMunicipio,$txtEstado,$txtCP,$dblLat,$dblLong,$FBID,$TWID,$txtTelefono,$txtEmail,$datetime,$txtBuscador,$txtWebsite,$txtDescripcion,$intCategoria,$intEmpresa);
	$mysqli->query($query);
}

if(isset($_POST['video'])&&$_POST['video']!=""){
$hash=$mysqli->real_escape_string($_POST['video']);
$query=sprintf("INSERT INTO tblfotos (intTipo,txtHash,intEmpresa,intActivo,datetime) VALUES (1,'%s',%s,1,'%s')",$hash,$intEmpresa,getUTC());
$mysqli->query($query);
header("Location: /edit.php?e=".$intEmpresa);exit;
}

function crop($source_path,$dest,$w,$h=0){
	if($h==0){
		$h=$w;
	}
	list($source_width, $source_height, $source_type) = getimagesize($source_path);
	switch ($source_type) {
	    case IMAGETYPE_GIF:
	        $source_gdim = imagecreatefromgif($source_path);
	        break;
	    case IMAGETYPE_JPEG:
	        $source_gdim = imagecreatefromjpeg($source_path);
	        break;
	    case IMAGETYPE_PNG:
	        $source_gdim = imagecreatefrompng($source_path);
	        break;
	}
	$source_aspect_ratio = $source_width / $source_height;
	$desired_aspect_ratio = $w / $h;

	if ($source_aspect_ratio > $desired_aspect_ratio) {
	    $temp_height = $h;
	    $temp_width = ( int ) ($h * $source_aspect_ratio);
	} else {
	    $temp_width = $w;
	    $temp_height = ( int ) ($w / $source_aspect_ratio);
	}
	$temp_gdim = imagecreatetruecolor($temp_width, $temp_height);
	imagecopyresampled(
	    $temp_gdim,
	    $source_gdim,
	    0, 0,
	    0, 0,
	    $temp_width, $temp_height,
	    $source_width, $source_height
	);
	$x0 = ($temp_width - $w) / 2;
	$y0 = ($temp_height - $h) / 2;
	$desired_gdim = imagecreatetruecolor($w, $h);
	imagecopy(
	    $desired_gdim,
	    $temp_gdim,
	    0, 0,
	    $x0, $y0,
	    $w, $h
	);
	imagejpeg($desired_gdim,$dest);
	imagedestroy($desired_gdim);
	imagedestroy($temp_gdim);
	imagedestroy($source_gdim);
	return true;
}
if(isset($_FILES['img'])){
	$target_dir = "ff/";
	$uploadOk = 1;
	$imageFileType = pathinfo($_FILES["img"]["name"],PATHINFO_EXTENSION);
	$hash=sha1(getUTC().$intEmpresa.basename($_FILES["img"]["name"]));
	$target_file = $target_dir . $hash.".jpg";
	if(isset($_POST["submit"])) {
		$check = getimagesize($_FILES["img"]["tmp_name"]);
		if($check !== false) {
			echo "File is an image - " . $check["mime"] . ".";
			$uploadOk = 1;
		} else {
			echo "File is not an image.";
			$uploadOk = 0;
		}
	}
	if ($_FILES["img"]["size"] > 500000) {
		echo "Sorry, your file is too large.";
		$uploadOk = 0;
	}
	if($imageFileType != "jpg" &&  $imageFileType != "jpeg" ) {
		echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		$uploadOk = 0;
	}
	if ($uploadOk == 0) {
		echo "Sorry, your file was not uploaded.";
	} else {
		if (move_uploaded_file($_FILES["img"]["tmp_name"], $target_file)) {
			echo "The file ". basename( $_FILES["img"]["name"]). " has been uploaded.";
			if(crop($target_file,"ff/m/".$hash.".jpg",150)){
				$query=sprintf("INSERT INTO tblfotos (intTipo,txtHash,intEmpresa,intActivo,datetime) VALUES (0,'%s',%s,1,'%s')",$hash,$intEmpresa,getUTC());
				$mysqli->query($query);
			}
		} else {
			echo "Sorry, there was an error uploading your file.";
		}
	}
	header("Location: /edit.php?e=".$intEmpresa);exit;
}
$query=sprintf("SELECT tblempresas.*,tblcategorias.txtSEO AS txtSEOCat FROM tblempresas LEFT JOIN tblcategorias ON tblcategorias.intCategoria=tblempresas.intCategoria WHERE intEmpresa=%s",$intEmpresa);
$i=$mysqli->query($query);
$e=$i->fetch_assoc();
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<title><?php echo $e['txtEmpresa']; ?> - Kontact</title>
	<?php require_once("meta.php"); ?>
	<?php require_once("css.php"); ?>
	<?php require_once("js.php"); ?>
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
	<script src="/s/es_MX.js"></script>
	<script>tinymce.init({
		selector:'textarea',
		language_url: '/s/es_MX.js',
		theme: 'modern',
		plugins: [
		'advlist autolink lists charmap hr anchor pagebreak',
		'searchreplace visualblocks visualchars code fullscreen',
		'insertdatetime nonbreaking save table contextmenu directionality',
		'template paste textcolor colorpicker textpattern imagetools toc'
		],
		toolbar1: 'undo redo | insert | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image print preview media | forecolor backcolor emoticons | codesample',
		image_advtab: true
	});</script>
	<style type="text/css">
	.mce-tinymce{
		width: 90%!important;
		margin: 0 auto!important;
	}
	.removefile{
		background-color:red;
		color:white;
		font-weight:bold;
		font-size:12px;
		padding:4px;
		text-transform:uppercase;
		letter-spacing:0px;
		border-bottom: 3px solid #036;
	}
	.removefile:hover{
		background-color: #840000;
		cursor: pointer;
	}
	</style>
</head>
<body>
	<div id="container">
		<?php $ht=$e['txtEmpresa'];$iconh=$e['intCategoria'];$iconurl="/c/".$e['txtSEOCat']; require_once("header.php"); ?>
		<form id="register" method="POST">
			<input value="<?php echo $e['txtEmpresa']; ?>" name="nombre" type="text" placeholder="Nombre" required>
			<select name="categoria">
				<option value="24">Categoría...</option>
				<?php
				$query=sprintf("SELECT * FROM tblcategorias ORDER BY txtCategoria ASC");
				$categorias=$mysqli->query($query);
				$c=$categorias->fetch_assoc();
				do{
					echo '<option value="'.$c['intCategoria'].'"'.($c['intCategoria']==$e['intCategoria']?"selected":"").'>'.$c['txtCategoria'].'</option>';
				}while($c=$categorias->fetch_assoc());
				?>
			</select>
			<select name="estado">
				<option value="24">San Luis Potosí</option>
			</select>
			<input value="<?php echo $e['txtCalle']; ?>" name="calle" type="text" placeholder="Calle">
			<input value="<?php echo $e['txtNoExt']; ?>" name="noext" type="text" placeholder="No. Exterior">
			<input value="<?php echo $e['txtNoInt']; ?>" name="noint" type="text" placeholder="No. Interior">
			<input value="<?php echo $e['txtColonia']; ?>" name="colonia" type="text" placeholder="Colonia">
			<input value="<?php echo $e['txtMunicipio']; ?>" name="municipio" type="text" placeholder="Municipio">
			<input value="<?php echo $e['txtCP']; ?>" name="cp" type="text" placeholder="Código Postal">
			<input value="<?php echo $e['txtWebsite']; ?>" name="web" type="text" placeholder="Sitio Web">
			<input value="<?php echo $e['FBID']; ?>" name="fbid" type="text" placeholder="Facebook ID">
			<input value="<?php echo $e['TWID']; ?>" name="twid" type="text" placeholder="Twitter ID">
			<input value="<?php echo $e['txtTelefono']; ?>" name="tel" type="text" placeholder="Teléfonos">
			<input value="<?php echo $e['txtEmail']; ?>" name="email" type="email" placeholder="E-mail">
			<textarea name="descripcion"><?php echo $e['txtDescripcion']; ?></textarea>
			<input type="hidden" name="latitud" id="latitud" value="<?php echo $e['dblLat']; ?>">
			<input type="hidden" name="longitud" id="longitud" value="<?php echo $e['dblLong']; ?>"><br>
		
			<button class="btn" id="setcurrent"><i class="fa fa-map-marker"></i> Ubicación Actual</button>
			<div id="map" style="width:100%;height:300px;max-width:500px;margin: 10px auto;border:1px solid #bbb;"></div>
			<input type="hidden" name="edit" value="1">
			<input type="submit" value="Guardar" class="btn" style="width:auto;">
		</form>
		<div class="tac">
		<form method="POST" enctype="multipart/form-data">
		<input name="img" type="file">
		<input type="submit" class="btn" value="Agregar Imagen">
		</form>
		<form method="POST">
		<input autocomplete="off" name="video" type="text" placeholder="Youtube Video ID" required>
		<input type="submit" class="btn" value="Agregar Video">
		</form>
		</div>
		<?php
		$query=sprintf("SELECT * FROM tblfotos WHERE intEmpresa=%s",$e['intEmpresa']);
		$if=$mysqli->query($query);
		if($if->num_rows>0){ ?>
		<ul id="profile-media" class="media">
			<?php
			$f=$if->fetch_assoc();
			do{
				if($f['intTipo']==0){
				$img="/ff/m/".$f['txtHash'].".jpg";
				}else if($f['intTipo']==1){
				$img="http://img.youtube.com/vi/".$f['txtHash']."/default.jpg";
				}
				?>
			<li ref="<?php echo $f['intRegistro']; ?>" data-hash="<?php echo $f['txtHash']; ?>" data-tipo="<?php echo $f['intTipo']; ?>" style="background-image:url('<?php echo $img; ?>');"><div class="removefile" ref="<?php echo $f['intRegistro']; ?>">Eliminar</div></li>
			<?php }while($f=$if->fetch_assoc()); ?>
		</ul>
		<?php } ?>
	</div>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAl4aQuUYYqyIHtkOOYOHwV3LKcJBsIDrM&callback=initMap"
        async defer></script>
    <script type="text/javascript">
    var map; var marker;
    function initMap() {
		map = new google.maps.Map(document.getElementById('map'), {
		center: {lat:22.152131, lng:-100.977923},
		zoom: 16
		});
		marker = new google.maps.Marker({
		map: map,
		draggable: true
		});
		map.addListener('click', function(e) {
			console.log(e);
			$('#latitud').val(e.latLng.lat());
			$('#longitud').val(e.latLng.lng());
			marker.setPosition(e.latLng);
		});
		google.maps.event.addListener(marker, 'dragend', function (event) {
			$('#latitud').val(this.getPosition().lat());
			$('#longitud').val(this.getPosition().lng());
		});
	}
	$(document).ready(function() {
		$('#setcurrent').on("click",function(e){
			e.stopPropagation();
			e.preventDefault();
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function(position) {
					var pos = {
					lat: position.coords.latitude,
					lng: position.coords.longitude
					};
					$('#latitud').val(position.coords.latitude);
					$('#longitud').val(position.coords.longitude);
					map.setCenter(pos);
					marker.setPosition(pos);
				}, function(e) {
				//handleLocationError(true, infoWindow, map.getCenter());
				});
			}
		});
		$('.removefile').on('click', function(event) {
			event.preventDefault();
			var ref=$(this).attr("ref");
			$.ajax({
				url: '/s/remove_file.php',
				type: 'POST',
				data: {file: ref}
			})
			.done(function(data){
				$('#profile-media li[ref="'+ref+'"]').remove();
			});
			
		});
	});
    </script>
</body>
</html>
<script src="https://code.jquery.com/jquery-3.1.0.min.js" integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="/js.js"></script>
<?php if(!isset($_SESSION['IDUS'])){ ?>
<script>
logInWithFacebook = function(){
	FB.login(checkLoginStatus, {scope: 'email'});
	return false;
};
window.fbAsyncInit = function() {
	FB.init({
		appId: '697272787114779',
		cookie: true,
		version: 'v2.8'
	});
	//FB.getLoginStatus(checkLoginStatus);
};

function checkLoginStatus(response) {
	if(response && response.status == 'connected') {
		$.ajax({
		    url: '/s/login.php',
		    type: 'POST',
		    dataType: 'html'
		})
		.done(function(json) {
			window.location.href = window.location.href;
		});
	}
}
(function(d, s, id){
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) {return;}
js = d.createElement(s); js.id = id;
js.src = "//connect.facebook.net/en_US/sdk.js";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
<script type="text/javascript">
$(document).ready(function() {
	$('.sld').on('click', function(event) {
		event.preventDefault();
		$('#drf').show();
		$('#drwf').hide();
		$('#dialog-login').dialog("open");
	});
	$('.srd').on('click', function(event) {
		event.preventDefault();
		$('#drf').hide();
		$('#drwf').show();
		$('#dialog-login').dialog("open");
	});
	$('#dialog-login').dialog({
		autoOpen:false,
		resizable: false,
		height: "auto",
		width: 400,
		modal: true,
		dialogClass: 'noTitle'
	});
	$('#show-register').on('click', function(event) {
		event.preventDefault();
		$('#drf').toggle();
		$('#drwf').toggle();
	});
	$('#form-register').on('submit', function(event) {
		event.preventDefault();
		$.ajax({
			url: '/s/register.php',
			type: 'POST',
			data: $(this).serialize()
		})
		.done(function(data) {
			window.location.href=window.location.href;
		});
	});
	$('#form-login').on('submit', function(event) {
		event.preventDefault();
		$.ajax({
			url: '/s/logink.php',
			type: 'POST',
			data: $(this).serialize()
		})
		.done(function(data) {
			window.location.href=window.location.href;
		});
	});
});
</script>
<?php } ?>
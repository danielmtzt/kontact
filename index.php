<?php
require_once("s/db.php");
if(isset($_SESSION['IDUS'])){
$intUsuario=$_SESSION['IDUS'];
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<title>Kontact</title>
	<?php require_once("meta.php"); ?>
	<?php require_once("css.php"); ?>
	<?php require_once("js.php"); ?>
	<script type="text/javascript">
	$(document).ready(function() {
		$('#searchbox').keyup(function(event) {
			event.preventDefault();
			$('#mainlogo').addClass('searching');
			if($('#searchbox').val().length>3){
				$('#do-search').html('<i class="kon-spin fa-spinner"></i>');
				$.ajax({
					url: '/s/search.php',
					type: 'POST',
					dataType: 'json',
					data: {search: $('#searchbox').val()}
				})
				.done(function(data) {
					console.log(data);
					if(data.length>0){
						var r='';
						for (var i = 0; i < data.length; i++) {
							r+=sre(data[i]);
						};
						$('#search-results').html(r).show();
						setFavoritos();
					}else{
						$('#search-results').hide().html("");
					}
					
				})
				.fail(function(data) {
				})
				.always(function() {
					$('#do-search').html('<i class="kon-search"></i>');
				});
			}else{
				$('#search-results').hide();
			}
			
		});
		var cs=0;
		$('#pprev').on('click', function(event) {
			event.preventDefault();
			cs=cs-$('#hsvc').width();
			if(cs<0){cs=0;}
			$('#hsvc').animate({scrollLeft:cs},800);
		});
		$('#pnext').on('click', function(event) {
			event.preventDefault();			
			var tw=$('.blp:first li:first').outerWidth()*$('.blp:first li').length;
			var csn=cs+$('#hsvc').width();
			if(csn<tw){cs=csn;}
			$('#hsvc').animate({scrollLeft:cs},800);
		});
		setInterval(function(){
			var tw=$('.blp:first li:first').outerWidth()*$('.blp:first li').length;
			var csn=cs+$('#hsvc').width();
			if(csn<tw){cs=csn;}else{cs=0;}
			$('#hsvc').animate({scrollLeft:cs},800);
		},10*1000);
		
	});
	</script>
</head>
<body>
	<div id="container">
	<div id="main-search">
		<div style="text-align:right;font-size:12px;text-transform:uppercase;padding:5px;font-weight:bold;line-height:40px;">
			<a href="/addempresa.php">Ingresa tu Negocio</a>
			<?php if(isset($_SESSION['IDUS'])){ ?>
				<a href="/perfil" style="text-decoration:none;">
				<?php
				if(isset($_SESSION['FBID'])){
					$profileimg="http://graph.facebook.com/".$_SESSION['FBID']."/picture?type=square";
				}else{
					$profileimg="https://www.gravatar.com/avatar/".md5(strtolower(trim($_SESSION['email'])))."?s=50&d=mm";
				} ?>
				<img style="border:1px solid #000;border-radius:25px;width:38px;height:38px;display:inline-block;vertical-align:top;" src="<?php echo $profileimg; ?>">
				</a>
				<a href="/logout.php">Cerrar Sesión</a>
			<?php }else{ ?>
			/ <span class="srd">Regístrate</span> / <span class="sld">Inicia Sesión</span>
			<?php } ?>
		</div>
		<img id="mainlogo" src="/3.gif"><br>
		<div style="font-size:28px;margin-bottom:5px;font-weight:bold;">Kontact</div>
		<input id="searchbox" type="search"><button id="do-search"><i class="kon-search"></i></button>
		<div id="search-container">
			<ul id="search-results" class="search-results"></ul>
		</div>
	</div>
	<div id="g0"></div>
	<div id="main-categories">	
		<ul class="clr">
			<?php
			$query=sprintf("SELECT * FROM tblcategorias WHERE intEmpresas>0 ORDER BY txtCategoria ASC");
			$i=$mysqli->query($query);
			$r=$i->fetch_assoc();
			do{ ?>
			<a href="/c/<?php echo $r['txtSEO']; ?>"><li><i class="icon kon-<?php echo $r['intCategoria']; ?>"></i><br><span class="name"><?php echo $r['txtCategoria']; ?></span></li></a>
			<?php }while($r=$i->fetch_assoc()); ?>
		</ul>
		<?php
		if(isset($_SESSION['IDUS'])){
		$query=sprintf("SELECT tblempresas.* FROM tblfavoritos LEFT JOIN tblempresas ON tblempresas.intEmpresa=tblfavoritos.intEmpresa WHERE tblfavoritos.intUsuario=%s LIMIT 10",$intUsuario);
		$i=$mysqli->query($query);
		if($i->num_rows>0){ ?>
		<h3 style="color:white;"><i class="kon-star"></i> Favoritos</h3>
		<ul class="blr">
			<?php
			$e=$i->fetch_assoc();
			do{
			if($e['txtSEO']!=""){
				$seo=$e['txtSEO'];
			}else{
				$seo="e-".$e['intEmpresa'];
			}
			$profile="";
			if(file_exists("p/".$e['intEmpresa'].".jpg")){
				$profile="/p/".$e['intEmpresa'].".jpg";
			}else if($e['FBID']!=""){
				$profile="http://graph.facebook.com/".$e['FBID']."/picture?width=100";
			}
			?>
			<a href="/<?php echo $seo; ?>"><li><img src="<?php echo $profile; ?>"></li></a>
			<?php }while($e=$i->fetch_assoc()); ?>
		</ul>
		<?php } } ?>
		<?php
		if(isset($_SESSION['IDUS'])){
		$query=sprintf("SELECT tblempresas.*, MAX(intRegistro) FROM tblviews LEFT JOIN tblempresas ON tblempresas.intEmpresa=tblviews.intEmpresa WHERE tblviews.intUsuario=%s GROUP BY intEmpresa ORDER BY MAX(intRegistro) DESC LIMIT 10",$_SESSION['IDUS']);
		$i=$mysqli->query($query);
		if($i->num_rows>0){ ?>
		<h3 style="color:white;"><i class="kon-check-circle"></i> Visitados</h3>
		<ul class="blr">
			<?php
			$e=$i->fetch_assoc();
			do{
			if($e['txtSEO']!=""){
				$seo=$e['txtSEO'];
			}else{
				$seo="e-".$e['intEmpresa'];
			}
			$profile="";
			if(file_exists("p/".$e['intEmpresa'].".jpg")){
				$profile="/p/".$e['intEmpresa'].".jpg";
			}else if($e['FBID']!=""){
				$profile="http://graph.facebook.com/".$e['FBID']."/picture?width=100";
			}
			?>
			<a href="/<?php echo $seo; ?>"><li><img src="<?php echo $profile; ?>"></li></a>
			<?php }while($e=$i->fetch_assoc()); ?>
		</ul>
		<?php } } ?>	
	</div>
	<div id="g1"></div>

	<div id="hsv" style="background-color:white;">
	<div class="bps" id="pprev"><i class="kon-caret-left"></i></div>
	<div id="hsvc">
	<ul class="blp">
		<?php
		$query=sprintf("SELECT * FROM tblempresas");
		$i=$mysqli->query($query);
		if($i->num_rows>0){
		$e=$i->fetch_assoc();
		do{
		if($e['txtSEO']!=""){
			$seo=$e['txtSEO'];
		}else{
			$seo="e-".$e['intEmpresa'];
		}
		$profile="";
		if(file_exists("p/".$e['intEmpresa'].".jpg")){
			$profile="/p/".$e['intEmpresa'].".jpg";
		}else if($e['FBID']!=""){
			$profile="http://graph.facebook.com/".$e['FBID']."/picture?width=100";
		}
		?>
		<a href="/<?php echo $seo; ?>"><li><img src="<?php echo $profile; ?>"><div class="name"><?php echo $e['txtEmpresa']; ?></div><span class="bicon"><i class="kon-<?php echo $e['intEmpresa']; ?>"></i></span></li></a>
		<?php }while($e=$i->fetch_assoc()); } ?>
	</ul>
	</div>
	<div class="bps" id="pnext"><i class="kon-caret-right"></i></div>
	<div class="clear"></div>
	</div>
	</div>
	<?php require_once("dlogin.php"); ?>
</body>
</html>
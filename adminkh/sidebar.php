<aside class="main-sidebar">
<section class="sidebar">
<ul class="sidebar-menu">
	<li><a href="/adminkh"><i class="fa fa-home"></i> <span>Inicio</span></a></li>
	<li><a href="/adminkh/cuentas.php"><i class="fa fa-bank"></i> <span>Cuentas</span></a></li>
	<li><a href="/adminkh/vendedores.php"><i class="fa fa-user-secret"></i> <span>Vendedores</span></a></li>
	<li><a href="#"><i class="fa fa-shopping-cart"></i> <span>Ventas</span></a></li>
	<li><a href="/adminkh/cfdi.php"><i class="fa fa-file-text"></i> <span>Facturas</span></a></li>
	<li><a href="/adminkh/cfdi.php"><i class="fa fa-dropbox"></i> <span>Proveedores</span></a></li>
	<li><a href="/adminkh/categorias.php"><i class="fa fa-tags"></i> <span>Categorías</span></a></li>
	<li><a href="/adminkh/clientes.php"><i class="fa fa-users"></i> <span>Clientes</span></a></li>

	<?php /*
	<li class="treeview">
		<a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
		<span class="pull-right-container">
		<i class="fa fa-angle-left pull-right"></i>
		</span>
		</a>
		<ul class="treeview-menu">
		<li><a href="#">Link in level 2</a></li>
		<li><a href="#">Link in level 2</a></li>
		</ul>
	</li>
	*/ ?>
</ul>
</section>
</aside>
<?php
require_once("scripts/db.php");
?>
<!DOCTYPE html>
<html>
<head>
	<title>AdminLTE 2 | <?php echo $admin[1]; ?></title>
	<?php require_once("meta.php"); ?>
	<?php require_once("css.php"); ?>
</head>
<body class="<?php echo $admin[0]; ?>">
	<div class="wrapper">
		<?php require_once("header.php"); ?>
		<?php require_once("sidebar.php"); ?>
		<div class="content-wrapper">
			<section class="content-header">
			<h1>
			Page Header
			<small>Optional description</small>
			</h1>
			<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
			<li class="active">Here</li>
			</ol>
			</section>
			<section class="content">

			</section>
		</div>
		<?php require_once("sidebar_r.php"); ?>
	</div>
	<?php require_once("js.php"); ?>
</body>
</html>
<?php
require_once("scripts/db.php");
$intCategoria=$_GET['cat'];
if(isset($_POST['addcategoria'])&&trim($_POST['addcategoria'])!=""){
	$txtCategoria=$mysqli->real_escape_string(trim($_POST['addcategoria']));
	$query=sprintf("SELECT * FROM tbldivisiones WHERE txtDivision='%s'",$txtCategoria);
	$check=$mysqli->query($query);
	if($check->num_rows==0){
	$txtSEO=seo($txtCategoria);
	$query=sprintf("INSERT INTO tbldivisiones (txtDivision,txtSEO,intCategoria) VALUES ('%s','%s',%s)",$txtCategoria,$txtSEO,$intCategoria);
	$mysqli->query($query);
	}
	header("Location: /adminkh/divisiones.php?cat=".$intCategoria);exit;
}

$query=sprintf("SELECT * FROM tblcategorias WHERE intCategoria=%s",$intCategoria);
$categoria=$mysqli->query($query);
$c=$categoria->fetch_assoc();
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<title><?php echo $c['txtCategoria']; ?> | <?php echo $admin[1]; ?></title>
	<?php require_once("meta.php"); ?>
	<?php require_once("css.php"); ?>
</head>
<body class="<?php echo $admin[0]; ?>">
	<div class="wrapper">
		<?php require_once("header.php"); ?>
		<?php require_once("sidebar.php"); ?>
		<div class="content-wrapper">
			<section class="content-header">
			<h1><?php echo $c['txtCategoria']; ?></h1>
			<ol class="breadcrumb">
			<li><a href="/adminkh"><i class="fa fa-home"></i> Inicio</a></li>
			<li><a href="/adminkh/categorias.php"><i class="fa fa-tags"></i> Categorías</a></li>
			<li class="active"><?php echo $c['txtCategoria']; ?></li>
			</ol>
			</section>
			<section class="content">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Lista de Divisiones</h3>
				</div>
				<div class="box-body">

<form method="POST">
<input style="width:200px;display:inline-block;vertical-align:middle;" name="addcategoria" required type="text" class="form-control" placeholder="Nombre" autocomplete="off" autofocus>
<input type="submit" value="+ División" class="btn btn-primary">
</form>


<table class="table">
	<thead>
		<tr>
			<th>ID</th>
			<th>Nombre</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$query=sprintf("SELECT * FROM tbldivisiones WHERE intCategoria=%s ORDER BY txtDivision ASC",$intCategoria);
		$divisiones=$mysqli->query($query);
		if($divisiones->num_rows>0){
		$d=$divisiones->fetch_assoc();
		do{
		?>
		<tr class="trcat" ref="<?php echo $d['intDivision']; ?>">
			<td><?php echo $d['intDivision']; ?></td>
			<td><span class="catname" ref="<?php echo $d['intDivision']; ?>"><?php echo $d['txtDivision']; ?></span></td>
			<td class="tac"><div class="btn-group">
				<button type="button" class="btn btn-default editcat" data-toggle="modal" data-target="#edit-categoria" data-name="<?php echo $d['txtDivision']; ?>" ref="<?php echo $d['intDivision']; ?>"><i class="fa fa-edit"></i></button>
				<button type="button" class="btn btn-default removecat" ref="<?php echo $d['intDivision']; ?>" data-toggle="modal" data-target="#remove-categoria" data-name="<?php echo $d['txtDivision']; ?>"><i class="fa fa-remove"></i></button>
			</div></td>
		</tr>
		<?php }while($d=$divisiones->fetch_assoc()); } ?>
	</tbody>
</table>
				</div>
			</div>
			</section>
		</div>
		<?php require_once("sidebar_r.php"); ?>
	</div>
	<div class="modal fade" id="edit-categoria" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><i class="fa fa-edit"></i> Editar Categoría</h4>
				</div>
				<div class="modal-body">
					<input id="ec-text" type="text" class="form-control">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Cancelar</button>
					<button id="savecat" type="button" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="remove-categoria" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><i class="fa fa-remove"></i> Eliminar División</h4>
				</div>
				<div class="modal-body">
					¿Deseas eliminar <b><span id="strdel"></span></b> de la lista de Divisiones? Las empresas correspondientes se quedarán sin division.
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button id="removecat" type="button" class="btn btn-primary"><i class="fa fa-remove"></i> Eliminar</button>
				</div>
			</div>
		</div>
	</div>
	<?php require_once("js.php"); ?>
	<script type="text/javascript">
	$(document).ready(function() {
		$('.editcat').on('click', function(event) {
			event.preventDefault();
			$('#ec-text').val($(this).data("name")).attr("ref",$(this).attr("ref"));
		});
		$('#savecat').on('click', function(event) {
			event.preventDefault();
			var value=$('#ec-text').val();
			var ref=$('#ec-text').attr("ref");
			$.ajax({
				url: '/adminkh/scripts/update_division.php',
				type: 'POST',
				data: {cat: ref,name:value}
			})
			.done(function() {
				$('.catname[ref="'+ref+'"]').html(value);
				$('.editcat[ref="'+ref+'"]').data("name",value);
				$('#edit-categoria').modal('hide')
			});
			
		});
		$('.removecat').on('click', function(event) {
			event.preventDefault();
			$('#strdel').html($(this).data("name"));
			$('#removecat').attr("ref",$(this).attr("ref"));
		});
		$('#removecat').on('click', function(event) {
			event.preventDefault();
			var ref=$(this).attr("ref");
			$.ajax({
				url: '/adminkh/scripts/delete_division.php',
				type: 'POST',
				data: {cat: ref}
			})
			.done(function(data) {
				$('.trcat[ref="'+ref+'"]').remove();
				$('#remove-categoria').modal('hide');
			});	
		});
	});
	</script>
</body>
</html>
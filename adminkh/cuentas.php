<?php
require_once("scripts/db.php");
$intUsuario=1;
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<title>Cuentas | <?php echo $admin[1]; ?></title>
	<?php require_once("meta.php"); ?>
	<?php require_once("css.php"); ?>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
	<style type="text/css">
	#filtro-top .form-control,#form-pago .form-control{
		width: auto;
		display: inline-block;
	}
	#form-pago label{
		margin-right:5px; 
	}	
	</style>
</head>
<body class="<?php echo $admin[0]; ?>">
	<div class="wrapper">
		<?php require_once("header.php"); ?>
		<?php require_once("sidebar.php"); ?>
		<div class="content-wrapper">
			<section class="content-header">
			<h1><i class="fa fa-bank"></i> Cuentas</h1>
			<ol class="breadcrumb">
			<li><a href="/adminkh"><i class="fa fa-home"></i> Inicio</a></li>
			<li class="active">Cuentas</li>
			</ol>
			</section>
			<section class="content">

			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-map"></i> Movimientos</h3>
				</div>
				<div class="box-body">
					<div id="filtro-top" class="tac">
						<select id="filtro-month" class="filtro-mes form-control">
							<option value="01" selected>Enero</option>
							<option value="02">Febrero</option>
							<option value="03">Marzo</option>
							<option value="04">Abril</option>
							<option value="05">Mayo</option>
							<option value="06">Junio</option>
							<option value="07">Julio</option>
							<option value="08">Agosto</option>
							<option value="09">Septiembre</option>
							<option value="10">Octubre</option>
							<option value="11">Noviembre</option>
							<option value="12">Diciembre</option>
						</select>
						<select id="filtro-year" class="filtro-mes form-control">
							<option value="2017">2017</option>
							<option value="2016" selected>2016</option>
						</select>
						<div class="btn-group" style="margin-right:20px;">
						<button id="prevmonth" class="btn btn-default"><i class="fa fa-play fa-rotate-180"></i></button>
						<button id="nextmonth" class="btn btn-default"><i class="fa fa-play"></i></button>
						</div>
						<select id="filtro-cuenta" class="filtro-mes form-control">
							<?php
							$query=sprintf("SELECT * FROM tblcuentas WHERE intCuenta IN(SELECT intCuenta FROM tblcuentas_aut WHERE intUsuario=%s)",$intUsuario);
							$cuentas=$mysqli->query($query);
							$cuenta=$cuentas->fetch_assoc();
							do{ ?>
							<option value="<?php echo $cuenta['intCuenta']; ?>"><?php echo $cuenta['txtAlias']; ?></option>
							<?php }while($cuenta=$cuentas->fetch_assoc()); ?>
						</select>
						<div class="btn-group" style="margin-right:20px;">
							<button id="add-movimiento" class="btn btn-default" data-toggle="modal" data-target="#add-pago"><span data-toggle="tooltip" title="Agregar Movimiento"><i class="fa fa-plus"></i> <i class="fa fa-exchange"></i></span></button>
							<button class="btn btn-default" data-toggle="tooltip" title="Saldo Inicial"><i class="fa fa-play"></i> <i class="fa fa-dollar"></i></button>
						</div>
					</div>
					<div id="current-movimientos" style="margin-top:20px;"></div>
				</div>
			</div>

			</section>
		</div>
		<?php require_once("sidebar_r.php"); ?>
	</div>

	<div class="modal fade" id="remove-pago-confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><i class="fa fa-remove"></i> Eliminar Movimiento</h4>
					</div>
					<div class="modal-body">
					¿Seguro que deseas eliminar este movimiento? Esta acción no se podrá deshacer.
					</div>
					<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button id="remove-mov" type="button" class="btn btn-danger"><i class="fa fa-remove"></i> Eliminar</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="add-pago" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><i class="fa fa-info-circle"></i> Información del Pago</h4>
					</div>
					<form method="POST" id="form-pago" class="tac">
					<div class="modal-body">

<div style="width:100%;">
	<div style="width:50%;float:left;text-align:right;">
		<label>Movimiento:</label><select id="intER" name="intER" class="form-control">
			<option value="0">Depósito</option>
			<option value="1">Retiro</option>
		</select><br><br>
		<label>Fecha:</label><input name="datePago" type="date" required id="pago-fecha" value="<?php $dt=new DateTime();echo $dt->format("Y-m-d"); ?>" class="form-control"><br><br>
		<label>Monto:</label><input name="dblPago" type="text" required class="tar form-control" id="pago-monto" autocomplete="off"><br><br>
		<span class="hideretiro">
		<label>Facturas (ID,):</label><input class="tac form-control" name="txtCFDI" type="text" id="pago-cfdi" autocomplete="off"><br><br>
		</span>
		<label>Cita (ID):</label><input id="intCita" class="tac form-control" name="intCita" type="text" autocomplete="off"><br><br></span>
		<label>Metodo de Pago:</label>
		<select name="intMetodoPago" required id="pago-metodo" class="form-control">
		<option value="">Seleccionar...</option>
		<?php
		$metodos=getMetodo();
		foreach ($metodos as $key => $value) {
			if($key==1||$key==2||$key==3||$key==4){
				if($key!=2&&$key!=3&&$key!=1){
					$hr=' class="hideretiro"';
				}else{
					$hr="";
				}
		?>
		<option<?php echo $hr; ?> value="<?php echo $key ?>"><?php echo $value; ?></option>
		<?php } } ?>
		</select><br><br>
		<label>Cuenta: </label>
		<select name="intCtaPropia" required id="micuenta" class="form-control">
			<option value="">Seleccionar...</option>
			<?php $query=sprintf("SELECT * FROM tblcuentas");
			$cuentas=$mysqli->query($query);
			$cuenta=$cuentas->fetch_assoc();
			do{ ?>
			<option value="<?php echo $cuenta['intCuenta']; ?>"><?php echo $cuenta['txtAlias']; ?></option>
			<?php }while($cuenta=$cuentas->fetch_assoc()); ?>
		</select>
	</div>
	<div style="width:50%;float:left;text-align:right;">
		<input id="mode-edit" type="hidden" name="edit" value="0">
		<label># Cuenta:</label><input id="intNumCta" class="tac enablepago form-control" name="intNumCta" type="text"><br><br>
		<label># Cheque:</label><input id="intCheque" class="tac enablepago form-control" name="intCheque" type="text"><br><br>
		<label>Nombre:</label><input id="pago-benef" class="enablepago form-control" name="txtBeneficiario" type="text"><br><br>
		<label>RFC:</label><input id="pago-brfc" class="tac enablepago form-control" name="txtRFCBeneficiario" type="text"><br><br>
		<label>Banco:</label>
		<select name="intBancoTransf" class="enablepago form-control" id="intBancoTransf">
		<option value="">Seleccionar...</option>
		<?php $query=sprintf("SELECT * FROM tblbancos ORDER BY txtBanco ASC");
		$bancos=$mysqli->query($query);
		$banco=$bancos->fetch_assoc();
		do{ ?>
		<option value="<?php echo $banco['intBanco']; ?>"><?php echo $banco['txtBanco']; ?></option>
		<?php }while($banco=$bancos->fetch_assoc()); ?>
		</select><br>
		<div id="pago-id" style="padding:20px;font-size:18px;"></div>
	</div>

	<div class="clear"></div>
	<div class="tac">
		<br><label>Descripción:</label><input id="pago-obs" type="text" name="txtObservaciones" style="width:350px;" autocomplete="off" class="form-control"><br><br>
	</div>
</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<input type="submit" class="btn btn-primary" value="Guardar" />
				</div>
				</form>
			</div>
		</div>
	</div>
	
	<?php require_once("js.php"); ?>
	<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		function setDateFilter(){
			var today=new Date();
			var m=today.getMonth()+1;
			m=("00"+m).slice(-2);
			var y=today.getFullYear();
			$('#filtro-year').val(y);
			$('#filtro-month').val(m);
		}
		function getMovimientos(pend,search){
			$('#current-movimientos').html('<div class="tac" style="padding:20px;font-size:30px;"><i class="fa fa-spinner fa-spin"></i></div>');
			if(pend===undefined){
				pend=false;
			}
			if(search===undefined){
				search='';
			}
			var date=$('#filtro-year').val()+"-"+$('#filtro-month').val()+"-01";
			var idc=$('#filtro-cuenta').val();
			$.ajax({
				url: '/adminkh/bancos/movimientos.php',
				type: 'POST',
				dataType: 'html',
				data:{cuenta:idc,fecha:date,pendientes:pend,term:search}
			})
			.done(function(data) {
				$('#current-movimientos').html(data);
				$('#tblmovimientos').DataTable({
					"ordering": false,
					"info":     false,
					"iDisplayLength": 50,
					"drawCallback": function(settings, json) {
					    $('.hr').removeClass("hr");
					    if ($('#tblmovimientos').DataTable().page.info().pages>1) {
							$('.dataTables_paginate').show();
							$('.dataTables_length').show();
						} else {
							$('.dataTables_paginate').hide();
							$('.dataTables_length').hide();
						}
					  },
					"dom": '<"top"flp>rt<"bottom"p><"clear">',
					"language": {
					    "sProcessing":     "Procesando...",
					    "sLengthMenu":     "Mostrar _MENU_ movimientos",
					    "sZeroRecords":    "No se encontraron movimientos.",
					    "sEmptyTable":     "Ningún dato disponible en esta tabla",
					    "sInfo":           "Movimientos del _START_ al _END_ (Total: _TOTAL_)",
					    "sInfoEmpty":      "",
					    "sInfoFiltered":   "(Total: _MAX_ movimientos)",
					    "sInfoPostFix":    "",
					    "sSearch":         "Buscar:",
					    "sUrl":            "",
					    "sInfoThousands":  ",",
					    "sLoadingRecords": "Cargando...",
					    "oPaginate": {
					        "sFirst":    "Primero",
					        "sLast":     "Último",
					        "sNext":     "Siguiente",
					        "sPrevious": "Anterior"
					    },
					    "oAria": {
					        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
					    }
					}
				});
				$('.tag').off();
				$('.tag').on('contextmenu', function(event) {
					event.preventDefault();
					var tag=$(this);
					var p=$(this).attr("ref");
					if(confirm("¿Desea eliminar esta Etiqueta?")){
						$.ajax({
							url: '/adminkh/remove_label.php',
							type: 'POST',
							data: {pago: p},
						})
						.done(function() {
							tag.remove();
						});
					}
				});
				$('.pago-edit').off();
				$('.pago-edit').on('click', function(event) {
					event.preventDefault();
					$('.hideretiro').show();
					$("#pago-obs").prop({
						'required': false
					});
					$('#intER').val($(this).data("tipo"));
					$('#pago-id').html("ID: "+$(this).attr("ref"));
					$('#pago-fecha').val($(this).data("fecha"));
					$('#pago-cfdi').val($(this).data("cfdis"));
					$('#pago-monto').val($(this).data("monto"));
					$('#pago-benef').val($(this).data("beneficiario"));
					$('#pago-brfc').val($(this).data("brfc"));
					$('#pago-metodo').val($(this).data("metodo"));
					$('#micuenta').val($(this).data("micuenta"));
					$('#intBancoTransf').val($(this).data("banco"));
					$('#intCheque').val($(this).data('cheque'));
					$('#intNumCta').val($(this).data('cuenta'));
					$('#pago-obs').val($(this).data('observaciones'));
					$('#mode-edit').val($(this).attr("ref"));
					$('#intCita').val($(this).data('cita'));
					$('#pago-metodo').trigger('change');
				});
				$('.revision').off();
				$('.revision').on('click', function(event) {
					event.preventDefault();
					var rev=$(this);
					if(rev.hasClass('checked')){
						var upto=0;
					}else{
						var upto=1;
					}
					$.ajax({
						url: '/adminkh/bancos/checked.php',
						type: 'POST',
						data: {pago: $(this).attr("ref"),revisado:upto}
					})
					.done(function() {
						rev.toggleClass('checked');
					});

				});
				$('.saldocheck').off();
				$('.saldocheck').on('click', function(event) {
					event.preventDefault();
					var rev=$(this);
					if(rev.hasClass('checked')){
						var upto=0;
					}else{
						var upto=1;
					}
					$.ajax({
						url: '/adminkh/bancos/saldochecked.php',
						type: 'POST',
						data: {pago: $(this).attr("ref"),revisado:upto}
					})
					.done(function() {
						rev.toggleClass('checked');
					})
					.fail(function() {
						console.log("error");
					})
					.always(function() {
						console.log("complete");
					});

				});
				$('.pago-remove').off();
				$('.pago-remove').on('click', function(event) {
					event.preventDefault();
					$("#remove-pago-confirm").data("pagodelete",$(this).attr("ref"));
				});
				$('.etiquetar').off();
				$('.etiquetar').on('click', function(event) {
					event.preventDefault();
					var pago=$(this).attr("ref");
					var datac=$('body').data("categorias");

					$('.select-cats').html("").hide();
					$('.select-tags').html("").hide();
					var html='';
					for (var i = 0; i < datac.length; i++) {
						var cat=datac[i];
						html+='<div class="tag ts" ref="'+cat.intCategoria+'">'+cat.txtCategoria+'</div>';
					};
					$('.select-cats[ref="'+pago+'"]').html(html).show();
					$('.ts').off();
					$('.ts').on('click', function(event) {
						event.preventDefault();
						var ref=$(this).attr("ref");
						saveCat(pago,ref,0,$(this));
						$.ajax({
							url: '/adminkh/get_etiquetas.php',
							type: 'POST',
							dataType: 'json',
							data: {categoria: ref}
						})
						.done(function(data) {
							var html='';
							if(data[0]!=null){
							for (var i = 0; i < data.length; i++) {
								var cat=data[i];
								html+='<div class="tag ts2" ref="'+cat.intEtiqueta+'">'+cat.txtEtiqueta+'</div>';
							};
							}
							$('.ts2').off();
							if(html!=""){
							$('.select-tags[ref="'+pago+'"]').html(html).show();
							$('.ts2').on('click', function(event) {
								event.preventDefault();
								var ref=$(this).attr("ref");
								saveCat(pago,ref,1,$(this));
							});
							}else{
								$('.select-tags[ref="'+pago+'"]').html(html).hide();
							}
						});

					});
				});
			});
		}
		setDateFilter();
		getMovimientos();
		$('.filtro-mes').on('change', function(event) {
			//event.preventDefault();
			getMovimientos();
		});
		$("#prevmonth").on('click', function(event) {
			event.preventDefault();
			if($('#filtro-month option:selected').is(":first-child")){
				var month=$('#filtro-month option:last').val();
				if($('#filtro-year option:selected').is(":last-child")){
					var year=$('#filtro-year > option:first').val();
				}else{
					var year=$('#filtro-year > option:selected').next('option').val();
				}
			}else{
				var month=$('#filtro-month > option:selected').prev('option').val();
			}
			$('#filtro-month').val(month);
			if(year!=undefined){
			$('#filtro-year').val(year);
			}
			getMovimientos();
		});

		$("#nextmonth").on('click', function(event) {
			event.preventDefault();
			if($('#filtro-month option:selected').is(":last-child")){
				var month=$('#filtro-month option:first').val();
				if($('#filtro-year option:selected').is(":first-child")){
					var year=$('#filtro-year > option:last').val();
				}else{
					var year=$('#filtro-year > option:selected').prev('option').val();
				}
			}else{
				var month=$('#filtro-month > option:selected').next('option').val();
			}
			$('#filtro-month').val(month);
			if(year!=undefined){
			$('#filtro-year').val(year);
			}
			getMovimientos();
		});
		$('#form-pago').on('submit', function(event) {
			event.preventDefault();
			$.ajax({
				url: '/adminkh/bancos/add_pago.php',
				type: 'POST',
				data: $('#form-pago').serialize()
			})
			.done(function() {
				$('#add-pago').modal('toggle');
				getMovimientos();
				$('#form-pago')[0].reset();
			});
		});
		$('#add-movimiento').on('click', function(event) {
			event.preventDefault();
			$('#form-pago')[0].reset();
			$('.hideretiro').hide();
			$("#pago-obs").prop({
				'required': true
			});
			$('#mode-edit').val("0");
			$('#pago-metodo').trigger('change');
		});
		$('#remove-mov').on('click', function(event) {
			event.preventDefault();
			$.ajax({
				url: '/adminkh/bancos/remove_pago.php',
				type: 'POST',
				dataType: 'html',
				data: {pago: $("#remove-pago-confirm").data("pagodelete")}
			})
			.done(function() {
				$('#remove-pago-confirm').modal('toggle');
				getMovimientos();
			});
		});
	});
	</script>
</body>
</html>
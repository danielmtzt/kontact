<?php
require_once("scripts/db.php");
if(isset($_POST['addcategoria'])&&trim($_POST['addcategoria'])!=""){
	$txtCategoria=$mysqli->real_escape_string(trim($_POST['addcategoria']));
	$hash = substr(md5(microtime()),rand(0,26),3);
	$query=sprintf("SELECT * FROM tblvendedores WHERE txtID='%s'",$hash);
	$check=$mysqli->query($query);
	if($check->num_rows==0){
	$txtSEO=seo($txtCategoria);
	$query=sprintf("INSERT INTO tblvendedores (txtVendedor,txtID) VALUES ('%s','%s')",$txtCategoria,$hash);
	$mysqli->query($query);
	}
	header("Location: /adminkh/vendedores.php");exit;
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<title>Clientes | <?php echo $admin[1]; ?></title>
	<?php require_once("meta.php"); ?>
	<?php require_once("css.php"); ?>
</head>
<body class="<?php echo $admin[0]; ?>">
	<div class="wrapper">
		<?php require_once("header.php"); ?>
		<?php require_once("sidebar.php"); ?>
		<div class="content-wrapper">
			<section class="content-header">
			<h1><i class="fa fa-users"></i> Clientes</h1>
			<ol class="breadcrumb">
			<li><a href="/adminkh"><i class="fa fa-home"></i> Inicio</a></li>
			<li class="active">Clientes</li>
			</ol>
			</section>
			<section class="content">

			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-map"></i> Mapa</h3>
				</div>
				<div class="box-body">
					<div id="map" style="width:100%;height:300px;"></div>
				</div>
			</div>
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-list"></i> Lista</h3>
				</div>
				<div class="box-body">

<form method="POST">
<input style="width:200px;display:inline-block;vertical-align:middle;" name="addcategoria" required type="text" class="form-control" placeholder="Nombre" autocomplete="off" autofocus>
<input type="submit" value="+ Vendedor" class="btn btn-primary">
</form>

<table class="table">
	<thead>
		<tr>
			<th class="tac">ID</th>
			<th class="tac" colspan="2">Vendedor</th>
			<th class="tac">Acciones</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$query=sprintf("SELECT * FROM tblvendedores ORDER BY txtVendedor ASC");
		$categorias=$mysqli->query($query);
		if($categorias->num_rows>0){
		$c=$categorias->fetch_assoc();
		do{
		?>
		<tr class="trcat" ref="<?php echo $c['intVendedor']; ?>">
			<td class="tac" style="vertical-align:middle;"><?php echo $c['intVendedor']; ?></td>
			<td><a href="/adminkh/historial.php?cat=<?php echo $c['intVendedor']; ?>" class="catname" ref="<?php echo $c['intCategoria']; ?>"><img class="profile-user-img img-responsive img-circle" style="width:60px;height:60px;display:inline-block;" src="http://graph.facebook.com/<?php echo $c['FBID']; ?>/picture?type=square"> <?php echo $c['txtVendedor']; ?></a></td>
			<td class="tac" width="120" style="vertical-align:middle;"><div class="btn-group">
				<button type="button" class="btn btn-default editcat" data-toggle="modal" data-target="#edit-categoria" data-name="<?php echo $c['txtCategoria']; ?>" ref="<?php echo $c['intCategoria']; ?>"><i class="fa fa-edit"></i></button>
				<button type="button" class="btn btn-default removecat" ref="<?php echo $c['intCategoria']; ?>" data-toggle="modal" data-target="#remove-categoria" data-name="<?php echo $c['txtCategoria']; ?>"><i class="fa fa-remove"></i></button>
			</div></td>
		</tr>
		<?php }while($c=$categorias->fetch_assoc()); } ?>
	</tbody>
</table>
			
				</div>
			</div>
			</section>
		</div>
		<?php require_once("sidebar_r.php"); ?>
	</div>
	<div class="modal fade" id="edit-categoria" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><i class="fa fa-edit"></i> Editar Categoría</h4>
				</div>
				<div class="modal-body">
					<input id="ec-text" type="text" class="form-control">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Cancelar</button>
					<button id="savecat" type="button" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="remove-categoria" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><i class="fa fa-remove"></i> Eliminar Categoría</h4>
				</div>
				<div class="modal-body">
					¿Deseas eliminar <b><span id="strdel"></span></b> de la lista de Categorías? Se eliminarán todas las divisiones de esta categoría y las empresas correspondientes se quedarán sin categoría.
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button id="removecat" type="button" class="btn btn-primary"><i class="fa fa-remove"></i> Eliminar</button>
				</div>
			</div>
		</div>
	</div>
	<?php require_once("js.php"); ?>
	<script type="text/javascript">
	$(document).ready(function() {
		$('.editcat').on('click', function(event) {
			event.preventDefault();
			$('#ec-text').val($(this).data("name")).attr("ref",$(this).attr("ref"));
		});
		$('.removecat').on('click', function(event) {
			event.preventDefault();
			$('#strdel').html($(this).data("name"));
			$('#removecat').attr("ref",$(this).attr("ref"));
		});
		$('#savecat').on('click', function(event) {
			event.preventDefault();
			var value=$('#ec-text').val();
			var ref=$('#ec-text').attr("ref");
			$.ajax({
				url: '/adminkh/scripts/update_categoria.php',
				type: 'POST',
				data: {cat: ref,name:value}
			})
			.done(function(data) {
				$('.catname[ref="'+ref+'"]').html(value);
				$('.editcat[ref="'+ref+'"]').data("name",value);
				$('#edit-categoria').modal('hide');
			});	
		});
		$('#removecat').on('click', function(event) {
			event.preventDefault();
			var ref=$(this).attr("ref");
			$.ajax({
				url: '/adminkh/scripts/delete_categoria.php',
				type: 'POST',
				data: {cat: ref}
			})
			.done(function(data) {
				$('.trcat[ref="'+ref+'"]').remove();
				$('#remove-categoria').modal('hide');
			});	
		});
	});
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAl4aQuUYYqyIHtkOOYOHwV3LKcJBsIDrM&callback=initMap"
        async defer></script>
	<script type="text/javascript">
	var map;
	function initMap() {
		map = new google.maps.Map(document.getElementById('map'), {
		center: {lat:22.152131, lng:-100.977923},
		zoom: 13
		});
		marker = new google.maps.Marker({
		map: map,
		draggable: true
		});
		var location =[<?php
		$query=sprintf("SELECT * FROM tblcheckin LEFT JOIN tblvendedores ON tblvendedores.intVendedor=tblcheckin.intVendedor WHERE intRegistro IN (SELECT MAX(intRegistro) FROM tblcheckin GROUP BY intVendedor)");
		$marcadores=$mysqli->query($query);
		$ms="";
		$m=$marcadores->fetch_assoc();
		do{$ms.="['<div id=\"content\"><div id=\"bodyContent\"><b>".$m['txtVendedor']."</b><br>".$m['datetime']."</div></div>',".$m['dblLatitud'].",".$m['dblLongitud'].",'http://graph.facebook.com/".$m['FBID']."/picture?type=square&width=30'],";}while($m=$marcadores->fetch_assoc());
		echo trim($ms,",");
		?>];

		var infowindow = new google.maps.InfoWindow();
    
        for (var i = 0; i < location.length; i++) {
    
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(location[i][1], location[i][2]),
                map: map,
                title: location[i][0],
                icon: location[i][3]
            });
    
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent(location[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i)); 
        }
		
	}
	</script>
</body>
</html>
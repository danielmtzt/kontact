<?php
require_once('../scripts/db.php');
if(!isset($_SESSION['IDUS'])){header("Location: logout.php");exit;}
$intUsuario=$_SESSION['IDUS'];
?>
<!DOCTYPE html>
<html>
<head>
	<title>Bancos</title>
	<?php require_once("meta.php"); ?>
	<?php require_once("css.php"); ?>
	<?php require_once("js.php"); ?>
	<script type="text/javascript">
	$(document).ready(function(){
$(".menu li").click(function(){
	var ref=$(this).attr("ref");
	$(".menu li").removeClass('selected');
	$(this).addClass('selected');
	$(".menu-bar").removeClass( "open" );
	$('.menu-bar[ref="'+ref+'"]').addClass( "open" );
});
	function setDateFilter(){
		var today=new Date();
		var m=today.getMonth()+1;
		m=("00"+m).slice(-2);
		var y=today.getFullYear();
		$('#filtro-year').val(y);
		$('#filtro-month').val(m);
	}
	setDateFilter();
	function getMovimientos(pend,search){
		$('#current-movimientos').html('<div class="tac" style="padding:20px;font-size:30px;"><i class="fa fa-spinner fa-spin"></i></div>');
		if(pend===undefined){
			pend=false;
		}
		if(search===undefined){
			search='';
		}
		var date=$('#filtro-year').val()+"-"+$('#filtro-month').val()+"-01";
		var idc=$('#filtro-cuenta').val();
		$.ajax({
			url: '/consultorio/bancos/movimientos.php',
			type: 'POST',
			dataType: 'html',
			data:{cuenta:idc,fecha:date,pendientes:pend,term:search}
		})
		.done(function(data) {
			$('#current-movimientos').html(data);
			$('.tag').off();
			$('.tag').on('contextmenu', function(event) {
				event.preventDefault();
				var tag=$(this);
				var p=$(this).attr("ref");
				if(confirm("¿Desea eliminar esta Etiqueta?")){
					$.ajax({
						url: '/consultorio/remove_label.php',
						type: 'POST',
						data: {pago: p},
					})
					.done(function() {
						tag.remove();
					})
					.fail(function() {
						console.log("error");
					})
					.always(function() {
						console.log("complete");
					});
				}
			});
			$('.pago-edit').off();
			$('.pago-edit').on('click', function(event) {
				event.preventDefault();
				$('.hideretiro').show();
				$("#pago-obs").prop({
					'required': false
				});
				$('#intER').val($(this).data("tipo"));
				$('#pago-id').html("ID: "+$(this).attr("ref"));
				$('#pago-fecha').val($(this).data("fecha"));
				$('#pago-cfdi').val($(this).data("cfdis"));
				$('#pago-monto').val($(this).data("monto"));
				$('#pago-benef').val($(this).data("beneficiario"));
				$('#pago-brfc').val($(this).data("brfc"));
				$('#pago-metodo').val($(this).data("metodo"));
				$('#micuenta').val($(this).data("micuenta"));
				$('#intBancoTransf').val($(this).data("banco"));
				$('#intCheque').val($(this).data('cheque'));
				$('#intNumCta').val($(this).data('cuenta'));
				$('#pago-obs').val($(this).data('observaciones'));
				$('#mode-edit').val($(this).attr("ref"));
				$('#intCita').val($(this).data('cita'));
				$('#add-pago').dialog("open");
				$('#pago-metodo').trigger('change');
			});
			$('.revision').off();
			$('.revision').on('click', function(event) {
				event.preventDefault();
				var rev=$(this);
				if(rev.hasClass('checked')){
					var upto=0;
				}else{
					var upto=1;
				}
				$.ajax({
					url: '/consultorio/bancos/checked.php',
					type: 'POST',
					data: {pago: $(this).attr("ref"),revisado:upto}
				})
				.done(function() {
					rev.toggleClass('checked');
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});

			});
			$('.saldocheck').off();
			$('.saldocheck').on('click', function(event) {
				event.preventDefault();
				var rev=$(this);
				if(rev.hasClass('checked')){
					var upto=0;
				}else{
					var upto=1;
				}
				$.ajax({
					url: '/consultorio/bancos/saldochecked.php',
					type: 'POST',
					data: {pago: $(this).attr("ref"),revisado:upto}
				})
				.done(function() {
					rev.toggleClass('checked');
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});

			});
			$('.pago-remove').off();
			$('.pago-remove').on('click', function(event) {
				event.preventDefault();
				$("#remove-pago-confirm").data("pagodelete",$(this).attr("ref"));
				$('#remove-pago-confirm').dialog("open");
			});
			$('.etiquetar').off();
			$('.etiquetar').on('click', function(event) {
				event.preventDefault();
				var pago=$(this).attr("ref");
				var datac=$('body').data("categorias");

				$('.select-cats').html("").hide();
				$('.select-tags').html("").hide();
				var html='';
				for (var i = 0; i < datac.length; i++) {
					var cat=datac[i];
					html+='<div class="tag ts" ref="'+cat.intCategoria+'">'+cat.txtCategoria+'</div>';
				};
				$('.select-cats[ref="'+pago+'"]').html(html).show();
				$('.ts').off();
				$('.ts').on('click', function(event) {
					event.preventDefault();
					var ref=$(this).attr("ref");
					saveCat(pago,ref,0,$(this));
					$.ajax({
						url: '/consultorio/get_etiquetas.php',
						type: 'POST',
						dataType: 'json',
						data: {categoria: ref}
					})
					.done(function(data) {
						var html='';
						if(data[0]!=null){
						for (var i = 0; i < data.length; i++) {
							var cat=data[i];
							html+='<div class="tag ts2" ref="'+cat.intEtiqueta+'">'+cat.txtEtiqueta+'</div>';
						};
						}
						$('.ts2').off();
						if(html!=""){
						$('.select-tags[ref="'+pago+'"]').html(html).show();
						$('.ts2').on('click', function(event) {
							event.preventDefault();
							var ref=$(this).attr("ref");
							saveCat(pago,ref,1,$(this));
						});
						}else{
							$('.select-tags[ref="'+pago+'"]').html(html).hide();
						}
					})
					.fail(function() {
						console.log("error");
					})
					.always(function() {
						console.log("complete");
					});

				});
			});
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
	}
	function saveCat(p,r,t,tag){
		$.ajax({
			url: '/consultorio/save_tag.php',
			type: 'POST',
			dataType: 'html',
			data: {pago:p,ref:r,type:t}
		})
		.done(function(data) {
			tag.fadeTo('slow', 0.4).fadeTo('slow', 1.0);
			$('.movtag[ref="'+p+'"]').html(data);
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});

	}
	getMovimientos();
	$('#show-pendientes').on('click', function(event) {
		event.preventDefault();
		$('tr[data-tags=1]').toggle();
	});
	$("#nextmonth").on('click', function(event) {
		event.preventDefault();
		var isLastElementSelected = $('#filtro-month > option:selected').index() == $('#filtro-month > option').length -1;
		if (!isLastElementSelected) {
			$('#filtro-month > option:selected').removeAttr('selected').next('option').attr('selected', 'selected');
		} else {
			$('#filtro-month > option:selected').removeAttr('selected');
			$('#filtro-month > option').first().attr('selected', 'selected');
			$('#filtro-year > option:selected').removeAttr('selected').prev('option').attr('selected', 'selected');
		}
		getMovimientos();
	});

	$("#prevmonth").on('click', function(event) {
		event.preventDefault();
		var isFirstElementSelected = $('#filtro-month > option:selected').index() == 0;
		if (!isFirstElementSelected) {
			$('#filtro-month > option:selected').removeAttr('selected').prev('option').attr('selected', 'selected');
		} else {
			$('#filtro-month > option:selected').removeAttr('selected');
			$('#filtro-month > option').last().attr('selected', 'selected');
			$('#filtro-year > option:selected').removeAttr('selected').next('option').attr('selected', 'selected');
		}
		getMovimientos();
	});
	$('#add-pago').dialog({
		autoOpen:false,
		modal:true,
		resizable: false,
		width: 720
	});
	$('#add-retiro').on('click', function(event) {
		event.preventDefault();
		$('#form-pago')[0].reset();
		$('.hideretiro').hide();
		$("#pago-obs").prop({
			'required': true
		});
		$('#mode-edit').val("0");
		$('#add-pago').dialog("open");
		$('#pago-metodo').trigger('change');
	});
	
	$('#pago-metodo').on('change', function(event) {
		event.preventDefault();
		setInputsMetodo();
	});
	$('#form-pago').on('submit', function(event) {
		event.preventDefault();
		$.ajax({
			url: '/consultorio/bancos/add_pago.php',
			type: 'POST',
			data: $('#form-pago').serialize()
		})
		.done(function(data) {
			console.log(data);
			$('#add-pago').dialog("close");
			getMovimientos();
			$('#form-pago')[0].reset();
		})
		.fail(function() {

		});
	});

	$('#remove-pago-confirm').dialog({
		autoOpen:false,
		modal:true,
		resizable: false,
		width: 720,
		buttons: {
			"Eliminar":function(){
				var d=$(this);
				$.ajax({
					url: '/consultorio/bancos/remove_pago.php',
					type: 'POST',
					dataType: 'html',
					data: {pago: $("#remove-pago-confirm").data("pagodelete")}
				})
				.done(function() {
					d.dialog("close");
					getMovimientos();
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
			},
			"Cancelar":function(){
				$(this).dialog("close");
			}
		}
	});
	$('#add-saldo-btn').on('click', function(event) {
		event.preventDefault();
		$('#add-saldo').dialog("open");
	});
	$('#add-saldo').dialog({
		autoOpen:false,
		modal:true,
		resizable: false,
		width: 720,
		buttons: {
			"Guardar":function(){
				var d=$(this);
				var date=$('#filtro-year').val()+"-"+$('#filtro-month').val()+"-01";
				$.ajax({
					url: '/consultorio/bancos/add_saldo.php',
					type: 'POST',
					dataType: 'html',
					data: {fecha:date,monto: $("#add-saldo-monto").val(),cuenta:$('#filtro-cuenta').val()}
				})
				.done(function() {
					d.dialog("close");
					getMovimientos();
					$("#add-saldo-monto").val("");
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
			},
			"Cancelar":function(){
				$(this).dialog("close");
			}
		}
	});
	$('#refresh').on('click', function(event) {
		event.preventDefault();
		getMovimientos();
	});
	$('.filtro-mes').on('change', function(event) {
		event.preventDefault();
		getMovimientos();
	});
	$('#search-mov').on('keyup', function(event) {
		event.preventDefault();
		if($('#search-mov').val().length>3){
			getMovimientos(false,$(this).val());
		}
	});
	});
	</script>
	<style type="text/css">
	.revision,.saldocheck{
		cursor: pointer;
	}
	.revision.checked{
		background: green;
    	color: #fff;
	}
	.saldocheck.checked{
		background: #267fdd;
    	color: #fff;
	}
	</style>
</head>
<?php
$query=sprintf("SELECT * FROM tblcategorias ORDER BY intTipo DESC,txtCategoria ASC");
$info=$mysqli->query($query);
$row=$info->fetch_assoc();
$results=array();
do{
	array_push($results, $row);
}while($row=$info->fetch_assoc());
$categorias=json_encode($results);
?>
<body data-categorias='<?php echo $categorias; ?>'>
	<?php require_once("header.php"); ?>
	<div class="content">
	<div class="noprint">
	<h3>Bancos</h3>
	<select id="filtro-month" class="filtro-mes">
		<option value="01" selected>Enero</option>
		<option value="02">Febrero</option>
		<option value="03">Marzo</option>
		<option value="04">Abril</option>
		<option value="05">Mayo</option>
		<option value="06">Junio</option>
		<option value="07">Julio</option>
		<option value="08">Agosto</option>
		<option value="09">Septiembre</option>
		<option value="10">Octubre</option>
		<option value="11">Noviembre</option>
		<option value="12">Diciembre</option>
	</select>
	<select id="filtro-year" class="filtro-mes">
		<option value="2016" selected>2016</option>
		<option value="2015">2015</option>
		<option value="2014">2014</option>
	</select>
	<select id="filtro-cuenta" class="filtro-mes">
		<?php
		$query=sprintf("SELECT * FROM tblcuentas WHERE intCuenta IN(SELECT intCuenta FROM tblcuentas_aut WHERE intUsuario=%s) AND intCuenta NOT IN (13,14)",$intUsuario);
		$cuentas=$mysqli->query($query);
		$cuenta=$cuentas->fetch_assoc();
		do{ ?>
		<option value="<?php echo $cuenta['intCuenta']; ?>"><?php echo $cuenta['txtAlias']; ?></option>
		<?php }while($cuenta=$cuentas->fetch_assoc()); ?>
	</select>

	<button id="prevmonth"><i class="fa fa-play fa-rotate-180"></i></button>
	<button id="nextmonth"><i class="fa fa-play"></i></button>

	<a href="/consultorio/cuentas.php"><button style="margin-left:20px;">Editar Cuentas</button></a>
	<button id="show-pendientes" style="margin-left:20px;">Pendientes&nbsp;&nbsp;<i class="fa fa-tag"></i></button>
	<button id="add-saldo-btn" style="margin-left:20px;">Saldo Inicial</button>
	<button id="add-retiro" style="margin-left:20px;">+ Movimiento</button>
	<button id="refresh" style="margin-left:20px;">Actualizar</button>
	<input id="search-mov" type="search" placeholder="Buscar..." style="margin-left:20px;">
	</div>
	<div id="current-movimientos" style="margin-top:20px;"></div>
	<div class="clear"></div>
	</div>

	<div id="add-pago" title="Información del Pago">
<form method="POST" id="form-pago" class="tac">
<div style="width:100%;">
	<div style="width:50%;float:left;text-align:right;">
		<select id="intER" name="intER">
			<option value="0">Depósito</option>
			<option value="1">Retiro</option>
		</select><br><br>
		<label>Fecha:</label><input name="datePago" type="date" required id="pago-fecha" value="<?php $dt=new DateTime();echo $dt->format("Y-m-d"); ?>"><br><br>
		<label>Monto:</label><input name="dblPago" type="text" required class="tar" id="pago-monto" autocomplete="off"><br><br>
		<span class="hideretiro">
		<label>Facturas (ID,):</label><input class="tac" name="txtCFDI" type="text" id="pago-cfdi" autocomplete="off"><br><br>
		</span>
		<label>Cita (ID):</label><input id="intCita" class="tac" name="intCita" type="text" autocomplete="off"><br><br></span>
		<label>Metodo de Pago:</label>
		<select name="intMetodoPago" required id="pago-metodo">
		<option value="">Seleccionar...</option>
		<?php
		$metodos=getMetodo();
		foreach ($metodos as $key => $value) {
			if($key==1||$key==2||$key==3||$key==4){
				if($key!=2&&$key!=3&&$key!=1){
					$hr=' class="hideretiro"';
				}else{
					$hr="";
				}
		?>
		<option<?php echo $hr; ?> value="<?php echo $key ?>"><?php echo $value; ?></option>
		<?php } } ?>
		</select><br><br>
		<label>Cuenta: </label>
		<select name="intCtaPropia" required id="micuenta">
			<option value="">Seleccionar...</option>
			<?php $query=sprintf("SELECT * FROM tblcuentas");
			$cuentas=$mysqli->query($query);
			$cuenta=$cuentas->fetch_assoc();
			do{ ?>
			<option value="<?php echo $cuenta['intCuenta']; ?>"><?php echo $cuenta['txtAlias']; ?></option>
			<?php }while($cuenta=$cuentas->fetch_assoc()); ?>
		</select>
	</div>
	<div style="width:50%;float:left;text-align:right;">
		<input id="mode-edit" type="hidden" name="edit" value="0">
		<label># Cuenta:</label><input id="intNumCta" class="tac enablepago" name="intNumCta" type="text"><br><br>
		<label># Cheque:</label><input id="intCheque" class="tac enablepago" name="intCheque" type="text"><br><br>
		<label>Nombre:</label><input id="pago-benef" class="enablepago" name="txtBeneficiario" type="text"><br><br>
		<label>RFC:</label><input id="pago-brfc" class="tac enablepago" name="txtRFCBeneficiario" type="text"><br><br>
		<label>Banco:</label>
		<select name="intBancoTransf" class="enablepago" id="intBancoTransf">
		<option value="">Seleccionar...</option>
		<?php $query=sprintf("SELECT * FROM tblbancos ORDER BY txtBanco ASC");
		$bancos=$mysqli->query($query);
		$banco=$bancos->fetch_assoc();
		do{ ?>
		<option value="<?php echo $banco['intBanco']; ?>"><?php echo $banco['txtBanco']; ?></option>
		<?php }while($banco=$bancos->fetch_assoc()); ?>
		</select><br>
		<div id="pago-id" style="padding:20px;font-size:18px;"></div>
	</div>

	<div class="clear"></div>
</div>

		<div class="tac">
			<br><label>Descripción:</label><input id="pago-obs" type="text" name="txtObservaciones" style="width:350px;" autocomplete="off"><br><br>
			<input type="submit" value="Guardar Pago">
		</div>
	</form>
	</div>

	<div id="remove-pago-confirm" title="Eliminar Pago">
		<div class="tac">¿Seguro que deseas eliminar este pago?</div>
	</div>
	<div id="add-saldo" title="Saldo Inicial">
		<div class="tac"><input class="tac" id="add-saldo-monto" type="text" name="saldo" placeholder="Monto Saldo Inicial"></div>
	</div>
</body>
</html>
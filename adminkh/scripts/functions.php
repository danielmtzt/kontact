<?php
function setGlobal(){
	global $mysqli;
	$query=sprintf("SELECT * FROM tbladmin");
	$i=$mysqli->query($query);
	$r=$i->fetch_assoc();
	$a=array();
	do{
		$a[]=$r['txtValue'];
	}while($r=$i->fetch_assoc());
	return $a;
}
function usp($intPago=0){
	global $mysqli;
	if($intPago!=0){
		$query=sprintf("SELECT * FROM tblpagos WHERE intPago=%s",$intPago);
	}else{
		$query=sprintf("SELECT * FROM tblpagos WHERE txtSearch=''");
	}
	$check=$mysqli->query($query);
	if($check->num_rows>0){
	$row=$check->fetch_assoc();
	do{
		$txtSearch=getDescripcion($row);
		$txtSearch.=" ".getTags($row['intPago'],1);
		$query=sprintf("UPDATE tblpagos SET txtSearch='%s' WHERE intPago=%s",$txtSearch,$row['intPago']);
		$mysqli->query($query);
	}while($row=$check->fetch_assoc());
	}
}
function getFecha($fecha,$type=0){
	if($fecha!=""){
		$meses=array("01"=>"ENE","02"=>"FEB","03"=>"MAR","04"=>"ABR","05"=>"MAY","06"=>"JUN","07"=>"JUL","08"=>"AGO","09"=>"SEP","10"=>"OCT","11"=>"NOV","12"=>"DIC");
		$mesesl=array("01"=>"Enero","02"=>"Febrero","03"=>"Marzo","04"=>"Abril","05"=>"Mayo","06"=>"Junio","07"=>"Julio","08"=>"Agosto","09"=>"Septiembre","10"=>"Octubre","11"=>"Noviembre","12"=>"Diciembre");
		$dt=new DateTime($fecha);
		if($type==0){
		$fecha=$dt->format("d")."-".$meses[$dt->format("m")]."-".$dt->format("y");
		}else if($type==1){
		$fecha=$dt->format("d-").$meses[$dt->format("m")].$dt->format("-y H:i A");
		}else if($type==2){
		$fecha=$dt->format("d ").$mesesl[$dt->format("m")].$dt->format(" Y");
		}else if($type==3){
		$fecha=$mesesl[$dt->format("m")]." ".$dt->format(" Y");
		}
		return $fecha;
	}else{
		return "";
	}
}
function getPago($pago){
	$d="";
	$d.=getMetodo($pago['intMetodoPago']);
	if($pago['intMetodoPago']!=1&&$pago['intNumCta']!=0){
	$d.=" ".$pago['intNumCta'];
	}
	if($pago['intMetodoPago']==2&&$pago['intCheque']!=0){
	$d.=" #".$pago['intCheque'];
	}
	return $d;
}
function getDescripcion($pago){
	global $mysqli;
	$d="";
	if($pago['txtCFDI']!=""){
		$cfdis=trim($pago['txtCFDI'],",");
		$query=sprintf("SELECT * FROM tblcfdi WHERE intRegistro IN (%s)",$cfdis);
		$info=$mysqli->query($query);
		$row=$info->fetch_assoc();
		do{
			if($row['intER']==1){
				$user=$row['txtEmisor'];
			}else{
				$user=$row['txtReceptor'];
			}
			$d.="F-".$row['strSerie'].$row['intFolio'];
			if($user!=""){
				$d.=" - ".$user;
			}else{
				$d.=" - PUBLICO EN GENERAL";
			}
			$d.="<br>";
		}while($row=$info->fetch_assoc());
		$d.=getPago($pago);
	}else{
		if($pago['txtBeneficiario']!=""){
		$d.=$pago['txtBeneficiario']."<br>";
		}
		$d.=getPago($pago)." ".$pago['txtObservaciones'];
	}
	return $d;
}
function getMetodo($intMetodo=0){
	$metodos=array(
	1=>"Efectivo",
	2=>"Cheque",
	3=>"Transferencia",
	4=>"Tarjetas de crédito",
	5=>"Monederos electrónicos",
	6=>"Dinero electrónico",
	7=>"Tarjetas digitales",
	8=>"Vales de despensa",
	9=>"Bienes",
	10=>"Servicio",
	11=>"Por cuenta de tercero",
	12=>"Dación en pago",
	13=>"Pago por subrogación",
	14=>"Pago por consignación",
	15=>"Condonación",
	16=>"Cancelación",
	17=>"Compensación",
	98=>"NA",
	99=>"Otros"
	);
	if($intMetodo==0){
		return $metodos;
	}else{
		return $metodos[$intMetodo];
	}
}
function getTags($intPago,$type=0){
	global $mysqli;
	$response="";
	$query=sprintf("SELECT tbltags.intTag,tblcategorias.txtCategoria, tbletiquetas.txtEtiqueta FROM tbltags LEFT JOIN tblcategorias ON tblcategorias.intCategoria=tbltags.intCategoria LEFT JOIN tbletiquetas ON tbletiquetas.intEtiqueta=tbltags.intEtiqueta WHERE intPago=%s LIMIT 1",$intPago);
	$tags=$mysqli->query($query);
	if($tags->num_rows>0){
		if($type==1){
			$tag=$tags->fetch_assoc();
			$response="";
			do{
				if($tag['txtEtiqueta']!=""){
					$txt=$tag['txtCategoria'].' - '.$tag['txtEtiqueta'];
				}else{
					$txt=$tag['txtCategoria'];
				}
				$response.=$txt;
			}while($tag=$tags->fetch_assoc());
		}else{
			$response='';
			$tag=$tags->fetch_assoc();
			do{
				if($tag['txtEtiqueta']!=""){
					$txt=$tag['txtCategoria'].' - '.$tag['txtEtiqueta'];
				}else{
					$txt=$tag['txtCategoria'];
				}
				$response.='<br><span class="label label-primary tag" ref="'.$tag['intTag'].'">'.$txt.'</span>';
			}while($tag=$tags->fetch_assoc());
		}
	}
	return $response;
}
function seo($text){
	$text = trim(htmlentities($text, ENT_QUOTES, 'UTF-8'));
	$text = strtolower($text);
	$patron = array (
	'/[\`\.\¿\?\-\!\¡:, ]+/' => '-',
	'/&amp;/' => '',
	'/&iquest;/'=>'',
	'/&iexcl;/'=>'',
	'/&agrave;/' => 'a',
	'/&egrave;/' => 'e',
	'/&igrave;/' => 'i',
	'/&ograve;/' => 'o',
	'/&ugrave;/' => 'u',
	'/&aacute;/' => 'a',
	'/&eacute;/' => 'e',
	'/&iacute;/' => 'i',
	'/&oacute;/' => 'o',
	'/&uacute;/' => 'u',
	'/&acirc;/' => 'a',
	'/&ecirc;/' => 'e',
	'/&icirc;/' => 'i',
	'/&ocirc;/' => 'o',
	'/&ucirc;/' => 'u',
	'/&atilde;/' => 'a',
	'/&etilde;/' => 'e',
	'/&itilde;/' => 'i',
	'/&otilde;/' => 'o',
	'/&utilde;/' => 'u',
	'/&auml;/' => 'a',
	'/&euml;/' => 'e',
	'/&iuml;/' => 'i',
	'/&ouml;/' => 'o',
	'/&uuml;/' => 'u',
	'/&auml;/' => 'a',
	'/&euml;/' => 'e',
	'/&iuml;/' => 'i',
	'/&ouml;/' => 'o',
	'/&uuml;/' => 'u',
	'/&aring;/' => 'a',
	'/&ntilde;/' => 'n',
	'/&ordf;/' => 'a',
	'/ordf;/' => 'a',
	'/impresion/'=>'imp',
	'/variante/'=>'var',
	'/&quot;/' => '',
	'/&ordm;/'=>'o',
	'/&deg;/'=>'',
	'/&acute;/'=>'-');
	$text = preg_replace(array_keys($patron),array_values($patron),$text);
	$simbolos = array("(", ")", "p/","c/","/","#","\\","\"","+","++","---","----","&039;");
	$text=str_replace($simbolos,"",$text);
	$simbolos = array("-y-", "-e-","-de-", "-la-", "-con-","-para-","-en-","-o-","--");
	$text=str_replace($simbolos,"-",$text);
	$text=trim($text,"-");
	return $text;
}
?>
<?php
require_once("scripts/db.php");
if(isset($_POST['addcategoria'])&&trim($_POST['addcategoria'])!=""){
	$txtCategoria=$mysqli->real_escape_string(trim($_POST['addcategoria']));
	$query=sprintf("SELECT * FROM tblcategorias WHERE txtCategoria='%s'",$txtCategoria);
	$check=$mysqli->query($query);
	if($check->num_rows==0){
	$txtSEO=seo($txtCategoria);
	$query=sprintf("INSERT INTO tblcategorias (txtCategoria,txtSEO) VALUES ('%s','%s')",$txtCategoria,$txtSEO);
	$mysqli->query($query);
	}
	header("Location: /adminkh/categorias.php");exit;
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<title>Categorias | <?php echo $admin[1]; ?></title>
	<?php require_once("meta.php"); ?>
	<?php require_once("css.php"); ?>
</head>
<body class="<?php echo $admin[0]; ?>">
	<div class="wrapper">
		<?php require_once("header.php"); ?>
		<?php require_once("sidebar.php"); ?>
		<div class="content-wrapper">
			<section class="content-header">
			<h1><i class="fa fa-tags"></i> Categorias</h1>
			<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-home"></i> Inicio</a></li>
			<li class="active">Categorías</li>
			</ol>
			</section>
			<section class="content">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Lista de Categorías</h3>
				</div>
				<div class="box-body">

<form method="POST">
<input style="width:200px;display:inline-block;vertical-align:middle;" name="addcategoria" required type="text" class="form-control" placeholder="Nombre" autocomplete="off" autofocus>
<input type="submit" value="+ Categoría" class="btn btn-primary">
</form>

<table class="table">
	<thead>
		<tr>
			<th>ID</th>
			<th>Nombre</th>
			<th>Icono</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$query=sprintf("SELECT * FROM tblcategorias ORDER BY txtCategoria ASC");
		$categorias=$mysqli->query($query);
		if($categorias->num_rows>0){
		$c=$categorias->fetch_assoc();
		do{
		?>
		<tr class="trcat" ref="<?php echo $c['intCategoria']; ?>">
			<td><?php echo $c['intCategoria']; ?></td>
			<td><a href="/adminkh/divisiones.php?cat=<?php echo $c['intCategoria']; ?>" class="catname" ref="<?php echo $c['intCategoria']; ?>"><?php echo $c['txtCategoria']; ?></a></td>
			<td nowrap><i class="iprev kon-<?php echo $c['intCategoria']; ?>"></i> <input type="checkbox"<?php if($c['intIcono']==1){echo " checked";} ?>></td>
			<td class="tac" width="120"><div class="btn-group">
				<button type="button" class="btn btn-default editcat" data-toggle="modal" data-target="#edit-categoria" data-name="<?php echo $c['txtCategoria']; ?>" ref="<?php echo $c['intCategoria']; ?>"><i class="fa fa-edit"></i></button>
				<button type="button" class="btn btn-default removecat" ref="<?php echo $c['intCategoria']; ?>" data-toggle="modal" data-target="#remove-categoria" data-name="<?php echo $c['txtCategoria']; ?>"><i class="fa fa-remove"></i></button>
			</div></td>
		</tr>
		<?php }while($c=$categorias->fetch_assoc()); } ?>
	</tbody>
</table>
			
				</div>
			</div>
			</section>
		</div>
		<?php require_once("sidebar_r.php"); ?>
	</div>
	<div class="modal fade" id="edit-categoria" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><i class="fa fa-edit"></i> Editar Categoría</h4>
				</div>
				<div class="modal-body">
					<input id="ec-text" type="text" class="form-control">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Cancelar</button>
					<button id="savecat" type="button" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="remove-categoria" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><i class="fa fa-remove"></i> Eliminar Categoría</h4>
				</div>
				<div class="modal-body">
					¿Deseas eliminar <b><span id="strdel"></span></b> de la lista de Categorías? Se eliminarán todas las divisiones de esta categoría y las empresas correspondientes se quedarán sin categoría.
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button id="removecat" type="button" class="btn btn-primary"><i class="fa fa-remove"></i> Eliminar</button>
				</div>
			</div>
		</div>
	</div>
	<?php require_once("js.php"); ?>
	<script type="text/javascript">
	$(document).ready(function() {
		$('.editcat').on('click', function(event) {
			event.preventDefault();
			$('#ec-text').val($(this).data("name")).attr("ref",$(this).attr("ref"));
		});
		$('.removecat').on('click', function(event) {
			event.preventDefault();
			$('#strdel').html($(this).data("name"));
			$('#removecat').attr("ref",$(this).attr("ref"));
		});
		$('#savecat').on('click', function(event) {
			event.preventDefault();
			var value=$('#ec-text').val();
			var ref=$('#ec-text').attr("ref");
			$.ajax({
				url: '/adminkh/scripts/update_categoria.php',
				type: 'POST',
				data: {cat: ref,name:value}
			})
			.done(function(data) {
				$('.catname[ref="'+ref+'"]').html(value);
				$('.editcat[ref="'+ref+'"]').data("name",value);
				$('#edit-categoria').modal('hide');
			});	
		});
		$('#removecat').on('click', function(event) {
			event.preventDefault();
			var ref=$(this).attr("ref");
			$.ajax({
				url: '/adminkh/scripts/delete_categoria.php',
				type: 'POST',
				data: {cat: ref}
			})
			.done(function(data) {
				$('.trcat[ref="'+ref+'"]').remove();
				$('#remove-categoria').modal('hide');
			});	
		});
	});
	</script>
</body>
</html>
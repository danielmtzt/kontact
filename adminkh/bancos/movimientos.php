<?php
require_once('../scripts/db.php');
usp();
if(isset($_POST['fecha'])){
	$dt=new DateTime($_POST['fecha']);
}else{
	$dt=new DateTime();
}
$year=$dt->format("Y");
$month=$dt->format("m");
$intCuenta=$_POST['cuenta'];
$cuentas=array(2,3,21,19);
if(array_search($intCuenta, $cuentas)!==false){
	$ss=true;
}else{
	$ss=false;
}
$periodo=$dt->format("Y-m-01");
if($_POST['term']!=""){
	$e=explode(' ', $_POST['term']);
	$txtSearch="";
	foreach ($e as $txts) {
		$txtSearch.=" txtSearch LIKE('%".$mysqli->real_escape_string($txts)."%') AND";
	}
	$txtSearch=trim($txtSearch," AND");
}else{
	$txtSearch="";
}
$today=new DateTime(NULL,new DateTimeZone("America/Mexico_City"));
if(isset($_POST['pendientes'])&&$_POST['pendientes']=="true"){
	$pendientes=true;
}else{
	$pendientes=false;
}
if($txtSearch!=""){
$query=sprintf("SELECT * FROM tblpagos WHERE %s AND intCuenta IN(SELECT intCuenta FROM tblcuentas_aut WHERE intUsuario=%s) ORDER BY datePago ASC,intPago ASC",$_SESSION['IDUS'],$txtSearch);
}else if($pendientes){
$query=sprintf("SELECT * FROM tblpagos WHERE txtCFDI='' ORDER BY datePago ASC,intPago ASC",$year,$month,$intCuenta);
}else{
$query=sprintf("SELECT * FROM tblpagos WHERE YEAR(datePago)='%s' AND MONTH(datePago)='%s' AND intCuenta=%s ORDER BY datePago ASC,intPago ASC",$year,$month,$intCuenta);
}
$info=$mysqli->query($query);
if($info->num_rows>0){
$pago=$info->fetch_assoc();
?>
<table id="tblmovimientos" class="table">
	<thead>
	<tr>
		<th>Fecha</th>
		<th>Descripción</th>
		<th class="tar">Depósitos</th>
		<th class="tar">Retiros</th>
		<?php if($ss){ ?>
		<th class="tar">Saldo</th>
		<?php } ?>
		<th class="noprint"></th>
	</tr>
	</thead>
	<tbody>
	<?php
	$query=sprintf("SELECT * FROM tblsaldos WHERE dateFecha='%s' AND dblSaldo!=0 AND intCuenta=%s LIMIT 1",$periodo,$intCuenta);
	$saldos=$mysqli->query($query);
	if($saldos->num_rows==1&&!$pendientes){
		$saldoi=$saldos->fetch_assoc();
		$saldo=$saldoi['dblSaldo'];
		?>
		<tr>
			<td class="tac" nowrap><?php echo getFecha($saldoi['dateFecha']); ?></td>
			<td>SALDO INICIAL</td>
			<td class="tar"></td>
			<td class="tar"></td>
			<td class="tar"><?php echo number_format($saldo,2); ?></td>
			<td class="noprint"></td>
		</tr>
		<?php
	}else{
		$saldo=0;
	}
	$depositos=0;$retiros=0;$i=0;
	do{$i++;
		$tags=getTags($pago['intPago']);
		$pagofecha=new DateTime($pago['datePago']);
		if($pagofecha>$today){
			$postfechado=true;
		}else{
			$postfechado=false;
		}
		if($pago['intTipo']==0){
		$deposito=$pago['dblPago'];
		$retiro=0;
		}else{
		$deposito=0;
		$retiro=$pago['dblPago'];
		}
		$depositos+=$deposito;
		$retiros+=$retiro;
		$saldo+=$deposito-$retiro;
		?>
	<tr<?php if($postfechado){echo ' style="font-weight:bold;"';} ?> data-tags="<?php if($tags!=""){echo "1";}else{echo "0";} ?>" <?php if($i>=50){ ?>class="hr"<?php } ?>>
	<td class="tac revision<?php if($pago['intRevisado']==1){echo " checked";} ?>" nowrap ref="<?php echo $pago['intPago']; ?>"><?php echo getFecha($pago['datePago']); ?></td>
	<td>
		<?php echo getDescripcion($pago); ?>
		<span class="movtag noprint" ref="<?php echo $pago['intPago']; ?>"><?php echo $tags; ?></span>
		<div class="select-cats" ref="<?php echo $pago['intPago']; ?>"></div>
		<div class="select-tags" ref="<?php echo $pago['intPago']; ?>"></div>
	</td>
	<td class="tar"><?php if($deposito>0){echo number_format($deposito,2);} ?></td>
	<td class="tar"><?php if($retiro>0){echo number_format($retiro,2);} ?></td>
	<?php if($ss){ ?>
	<td class="tar saldocheck<?php if($pago['intSaldo']==1){echo " checked";} ?>" ref="<?php echo $pago['intPago']; ?>"<?php if($saldo<0){echo ' style="color:red!important;"';} ?>><?php if(!$pendientes){echo number_format($saldo,2);} ?></td>
	<?php } ?>
	<td class="tac noprint" nowrap style="width:170px;">
		<div class="btn-group">
		<?php if($pago['txtCFDI']==""){ ?>
		<button class="btn btn-danger">
		<i class="fa fa-file-text-o red" data-title="CFDI Pendiente"></i>
		</button>
		<?php } ?>
		<button class="btn btn-default pago-edit" data-toggle="modal" data-target="#add-pago" data-title="Editar Pago" ref="<?php echo $pago['intPago']; ?>" data-monto="<?php echo $pago['dblPago']; ?>" data-beneficiario="<?php echo $pago['txtBeneficiario']; ?>" data-brfc="<?php echo $pago['txtRFCB']; ?>" data-cfdis="<?php echo trim($pago['txtCFDI'],","); ?>" data-fecha="<?php echo $pago['datePago']; ?>" data-cheque="<?php echo $pago['intCheque']; ?>" data-cuenta="<?php echo $pago['intNumCta']; ?>" data-metodo="<?php echo $pago['intMetodoPago']; ?>" data-micuenta="<?php echo $pago['intCuenta']; ?>" data-banco="<?php echo $pago['intBancoDestino']; ?>" data-observaciones="<?php echo $pago['txtObservaciones']; ?>" data-tipo="<?php echo $pago['intTipo']; ?>" data-cita="<?php echo $pago['intCita']; ?>"><i class="fa fa-edit"></i></button>
		<button class="btn btn-default pago-remove" data-toggle="modal" data-target="#remove-pago-confirm" ref="<?php echo $pago['intPago']; ?>">
		<i class="fa fa-remove"></i>
		</button>
		<?php if($tags==""){ ?>
		<button class="btn btn-primary">
		<i class="fa fa-tag etiquetar" ref="<?php echo $pago['intPago']; ?>"></i>
		</button>
		<?php } ?>
		</div>
	</td>
	</tr>
	<?php
	}while($pago=$info->fetch_assoc()); ?>
	</tbody>
	<tfoot>
	<tr style="font-weight:bold;">	
		<td></td>
		<td class="tar" >Total:</td>
		<td class="tar"><?php if($depositos>0){echo number_format($depositos,2);} ?></td>
		<td class="tar"><?php if($retiros>0){echo number_format($retiros,2);} ?></td>
		<?php if($ss){ ?>
		<td></td>
		<?php } ?>
		<td></td>
	</tr>
	</tfoot>
</table>
<?php }else{ ?>
<div class="tac">
	No hay movimientos para este periodo.
</div>
<?php } ?>
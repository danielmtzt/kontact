<?php
require_once("../s/db.php");
if(isset($_POST['cat'])){
	$cat=$mysqli->real_escape_string($_POST['cat']);
	$query=sprintf("INSERT INTO tblcategorias (txtCategoria) VALUES ('%s')",$cat);
	$mysqli->query($query);
	header("Location: categorias.php");exit;
}
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>
<body>
	<?php require_once("header.php"); ?>
	<form method="POST">
		<input type="text" required placeholder="Nombre" name="cat">
		<input type="submit" value="+ Categoría">
	</form>
	<table>
		<?php
		$query=sprintf("SELECT * FROM tblcategorias");
		$info=$mysqli->query($query);
		$row=$info->fetch_assoc();
		do{
		?>
		<tr>
			<td><?php echo $row['txtCategoria']; ?></td>
			<td><i class="fa fa-image"></i> <i class="fa fa-edit"></i> <i class="fa fa-remove"></i></td>
		</tr>
		<?php }while($row=$info->fetch_assoc()); ?>
	</table>
</body>
</html>
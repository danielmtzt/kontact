<?php
require_once("../s/db.php");
if(isset($_POST['vendedor'])&&isset($_POST['latitud'])&&isset($_POST['longitud'])){
	$response="error";
	$lat=$mysqli->real_escape_string($_POST['latitud']);
	$lon=$mysqli->real_escape_string($_POST['longitud']);
	if($lat!=0&&$lon!=0){
	$query=sprintf("SELECT intVendedor FROM tblvendedores WHERE txtID='%s'",$mysqli->real_escape_string($_POST['vendedor']));
	$check=$mysqli->query($query);
	if($check->num_rows==1){
		$v=$check->fetch_assoc();
		$intVendedor=$v['intVendedor'];
	$query=sprintf("INSERT INTO tblcheckin (intVendedor,dblLatitud,dblLongitud,datetime) VALUES ('%s','%s','%s','%s')",$intVendedor,$lat,$lon,getUTC());
	if($mysqli->query($query)){
		$response="success";
	}
	}
	}
	header("Location: /checkin/?".$response);exit;
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<title>Check-in Kontact</title>
	<link rel="icon" type="image/png" href="/r/favicon.png">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
	<script src="https://code.jquery.com/jquery-3.1.0.min.js" integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>
	<style type="text/css">
	*{
		box-sizing: border-box;
		font-family:'Roboto',sans-serif;
		font-size: 16px;
	}
	body{
		text-align: center;
		margin: 0;
		padding: 4%;
		background: #bfdfff;
	}
	input[type="text"]{
		padding: 6px 10px;
		text-align: center;
		border: 1px solid #000;
		border-radius: 5px;
	}
	button,input[type="submit"]{
		margin-top: 10px;
		font-size: 12px;
		border: 1px solid #000;
		border-radius: 5px;
		padding:4px 10px;
		background: #036;
		color: #FFF;
	}
	</style>
</head>
<body>
	<img src="/r/logo.png" style="width:100px;">
	<h3>Check-in Kontact</h3>
	<?php if(isset($_GET['success'])){ ?>
	<div style="margin-bottom:10px;font-size:12px;font-weight:bold;color:green;">Check-in enviado correctamente.</div>
	<?php } ?>
	<?php if(isset($_GET['error'])){ ?>
	<div style="margin-bottom:10px;font-size:12px;font-weight:bold;color:red;">Error al enviar Check-in. Revisa tu información.</div>
	<?php } ?>
	<form id="fv" method="POST" class="tac">
		<input type="text" name="vendedor" required placeholder="Vendedor" autocomplete="off"><br>
		<input type="hidden" name="latitud" id="latitud">
		<input type="hidden" name="longitud" id="longitud">
		<input type="submit" value="Enviar Ubicación">
<div id="map" style="border-radius:5px;width:100%;height:300px;max-width:500px;margin: 10px auto;border:1px solid #bbb;"></div>
	</form>
	<button class="btn" id="setcurrent"><i class="fa fa-map-marker"></i> Ubicación Actual</button>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAl4aQuUYYqyIHtkOOYOHwV3LKcJBsIDrM&callback=initMap"
        async defer></script>
<script type="text/javascript">
var map; var marker;
function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
	center: {lat:22.152131, lng:-100.977923},
	zoom: 16
	});
	marker = new google.maps.Marker({
	map: map,
	draggable: true
	});
	map.addListener('click', function(e) {
		console.log(e);
		$('#latitud').val(e.latLng.lat());
		$('#longitud').val(e.latLng.lng());
		marker.setPosition(e.latLng);
	});
	google.maps.event.addListener(marker, 'dragend', function (event) {
		$('#latitud').val(this.getPosition().lat());
		$('#longitud').val(this.getPosition().lng());
	});
}
$(document).ready(function() {
	function getLocation(e){
		if (navigator.geolocation) {
			$('#setcurrent').html('<i class="fa fa-spin fa-spinner"></i> Buscando...');
			navigator.geolocation.getCurrentPosition(function(position) {
				var pos = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
				};
				$('#latitud').val(position.coords.latitude);
				$('#longitud').val(position.coords.longitude);
				map.setCenter(pos);
				marker.setPosition(pos);
				$('#setcurrent').html('<i class="fa fa-map-marker"></i> Ubicación Actual');
			}, function(e) {
			//handleLocationError(true, infoWindow, map.getCenter());
			});
		}
	}
	$('#fv').on('submit', function(event) {
		if($('#latitud').val()==""&&$('#longitud').val()==""){
			event.preventDefault();
		}
	});
	$('#setcurrent').on("click",getLocation);
	getLocation(null);
});
</script>
</body>
</html>
<?php
require_once("s/db.php");
if(isset($_GET['e'])){
$intEmpresa=$_GET['e'];
$query=sprintf("SELECT tblempresas.*,tblcategorias.txtSEO AS txtSEOCat FROM tblempresas LEFT JOIN tblcategorias ON tblcategorias.intCategoria=tblempresas.intCategoria WHERE intEmpresa=%s",$intEmpresa);
}else{
$intEmpresa=$_GET['s'];
$query=sprintf("SELECT tblempresas.*,tblcategorias.txtSEO AS txtSEOCat FROM tblempresas LEFT JOIN tblcategorias ON tblcategorias.intCategoria=tblempresas.intCategoria WHERE txtSEO='%s'",$intEmpresa);
}
$i=$mysqli->query($query);
$e=$i->fetch_assoc();
view($intEmpresa);
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<title><?php echo $e['txtEmpresa']; ?> - Buscador RT</title>
	<?php require_once("meta.php"); ?>
	<?php require_once("css.php"); ?>
	<?php require_once("js.php"); ?>
	<script type="text/javascript">
	function computeTotalDistance(result) {
  var total = 0;
  var time= 0;
  var from=0;
  var to=0;
  var myroute = result.routes[0];
  for (var i = 0; i < myroute.legs.length; i++) {
    total += myroute.legs[i].distance.value;
    time +=myroute.legs[i].duration.text;
    from =myroute.legs[i].start_address;
    to =myroute.legs[i].end_address;


  }
  time = time.replace('hours','H');
  time = time.replace('mins','M');
  total = total / 1000.
  document.getElementById('from').innerHTML = 'De: '+from + '<br>A: '+to;
  document.getElementById('duration').innerHTML = 'Tiempo estimado: '+time;
  document.getElementById('total').innerHTML ='Distancia: '+Math.round(total)+"KM";
  document.getElementById('rutacont').style.display = 'block';
}
	function directions(map,end){
		if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
		var pos = {
		lat: position.coords.latitude,
		lng: position.coords.longitude
		};
		map.setCenter(pos);
		

		var directionsDisplay = new google.maps.DirectionsRenderer({
          map: map,
          draggable: true
        });

        directionsDisplay.addListener('directions_changed', function() {
		computeTotalDistance(directionsDisplay.getDirections());
		});

        // Set destination, origin and travel mode.
        var request = {
          destination: end,
          origin: pos,
          travelMode: 'DRIVING'
        };

        // Pass the directions request to the directions service.
        var directionsService = new google.maps.DirectionsService();
        directionsService.route(request, function(response, status) {
          if (status == 'OK') {
            // Display the route on the map.
            directionsDisplay.setDirections(response);
          }
        });

        }, function(e) {
		//handleLocationError(true, infoWindow, map.getCenter());
		});
    	}
	}
	function initMap() {
		var markers=JSON.parse(document.getElementById('map').getAttribute("data-markers"));
		var place={lat:parseFloat(markers[0][0]), lng:parseFloat(markers[0][1])};
		
		var map = new google.maps.Map(document.getElementById('map'), {
		center: place,
		zoom: 16
		});

		for (var i = 0; i < markers.length; i++) {
			var marker = new google.maps.Marker({
			map: map,
			position: {lat:parseFloat(markers[i][0]), lng:parseFloat(markers[i][1])},
			title: 'Hello World!'
			});
		};
		document.getElementById('ruta').addEventListener('click', function() { directions(map,place) }, false);
	}
	$(document).ready(function() {
		$('#profile-media li').on('click', function(event) {
			event.preventDefault();
			$('#overlay').show();
			var txt="";
			if($(this).data("tipo")==0){
				txt='<img src="/ff/'+$(this).data("hash")+'.jpg" />';
			}else if($(this).data("tipo")==1){
				txt='<iframe style="width:100%;height:100%;" src="https://www.youtube.com/embed/'+$(this).data("hash")+'" frameborder="0" allowfullscreen></iframe>';
			}
			$('#dialog-media').css("top",($(window).height()*0.1/2)+"px").html(txt).show();
			$('#dialog-media img').on('load', function(event) {
				event.preventDefault();
				$(this).css("margin-top",(($('#dialog-media').height()-$(this).height())/2)+"px");
			});
		});
		$('#overlay').on('click', function(event) {
			event.preventDefault();
			$('#overlay').hide();
			$('#dialog-media').hide();
		});
	});
	</script>
</head>
<body>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.7&appId=358644980900253";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<script>window.twttr = (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0],
    t = window.twttr || {};
  if (d.getElementById(id)) return t;
  js = d.createElement(s);
  js.id = id;
  js.src = "https://platform.twitter.com/widgets.js";
  fjs.parentNode.insertBefore(js, fjs);
 
  t._e = [];
  t.ready = function(f) {
    t._e.push(f);
  };
 
  return t;
}(document, "script", "twitter-wjs"));</script>
	<div id="container">
		<?php $ht=$e['txtEmpresa'];$iconh=$e['intCategoria'];$iconurl="/c/".$e['txtSEOCat']; require_once("header.php"); ?>
		<?php
			$class="";
			if(file_exists("c/".$e['intEmpresa'].".jpg")){$hasCover=true;}else{$hasCover=false;}
			if(file_exists("p/".$e['intEmpresa'].".jpg")||$e['FBID']!=""){$hasLogo=true;}else{$hasLogo=false;}
			if($hasLogo&&!$hasCover){
				$class="onlylogo";
			}else if(!$hasLogo&&$hasCover){
				$class="onlycover";
			}
			$profile="";
			if(file_exists("p/".$e['intEmpresa'].".jpg")){
				$profile="/p/".$e['intEmpresa'].".jpg";
			}else if($e['FBID']!=""){
				$profile="http://graph.facebook.com/".$e['FBID']."/picture?width=150";
			}
		?>
		<div id="profile-header" class="<?php echo $class;?>">
			<?php if($profile!=""){ ?><div class="logo"><img src="<?php echo $profile; ?>"></div><?php } if(file_exists("c/".$e['intEmpresa'].".jpg")){ ?><div class="cover"><img src="/c/<?php echo $e['intEmpresa']; ?>.jpg"></div><?php } ?>
			<div class="clear"></div>

			<ul class="pi">
				<?php if($e['FBID']!=""){ ?>
				<a target="_blank" href="https://facebook.com/<?php echo $e['FBID']; ?>"><li><i class="kon-facebook" style="color:#3b5998;"></i></li></a>
				<?php } ?>
				<?php if($e['TWID']!=""){ ?>
				<a target="_blank" href="https://twitter.com/<?php echo $e['TWID']; ?>"><li><i class="kon-twitter" style="color:#1da1f2;"></i></li></a>
				<?php } ?>
				<?php if($e['txtWebsite']!=""){ ?>
				<a target="_blank" href="<?php echo $e['txtWebsite']; ?>"><li><i class="kon-globe" style="color:#036;"></i></li></a>
				<?php } ?>
				<?php if($e['txtTelefono']!=""){ ?>
				<a href="tel:+<?php echo $e['txtTelefono']; ?>"><li><i class="kon-phone" style="color:#079e22;"></i></li></a>
				<?php } ?>
				<?php if($e['txtEmail']!=""){ ?>
				<a href="mailto:<?php echo $e['txtEmail']; ?>"><li><i class="kon-envelope" style="color:#ffb500;"></i></li></a>
				<?php } ?>
				<?php if(isset($_SESSION['IDUS'])){ ?>
				<li class="addfav<?php if(isFav($intEmpresa)){echo ' isfav';} ?>" ref="<?php echo $intEmpresa; ?>"><i class="kon-star"></i></li>
				<?php } ?>
				<?php if($e['dblRating']>0){ ?>
				<li style="font-size:18px;"><?php echo number_format($e['dblRating'],1); ?><i class="kon-heart" style="color:#8c0606;"></i></li>
				<?php } ?>
				<?php if($e['intVisitas']>0){ ?>
				<li class="stat" style="font-size:18px;"><span class="value"><?php echo $e['intVisitas']; ?></span><br><span class="name"><i class="kon-eye" style="color:#036;"></i></span></li>
				<?php } ?>
				<a href="/edit.php?e=<?php echo $e['intEmpresa']; ?>"><li><i class="kon-edit"></i></li></a>
			</ul>
			<?php if(getDireccion($e)!=""||$e['txtTelefono']!=""||$e['txtEmail']!=""){ ?>
			<div class="tac" style="background:white;border:3px solid #036;border-radius:5px;padding:5px;width:90%;margin:10px auto;line-height:30px;">
				<i class="kon-map-marker"></i> <?php echo getDireccion($e); ?><br>
				<?php if($e['txtTelefono']!=""){ ?>
				<i class="kon-phone"></i> <?php echo $e['txtTelefono']; ?><br>
				<?php } ?>
				<?php if($e['txtEmail']!=""){ ?>
				<i class="kon-envelope"></i> <?php echo $e['txtEmail']; ?>
				<?php } ?>
			</div>
			<?php } ?>
		</div>
		<?php if($e['txtDescripcion']!=""){ ?>
		<div id="g0"></div>
		<div style="background-color: #006dd9;padding:10px;color:#222;">
			<?php echo $e['txtDescripcion']; ?>
		</div>
		<div id="g1"></div>
		<?php } ?>
		<div style="background: url('/r/bgmain.png') #fff; padding: 20px 0;">
			<?php
			$query=sprintf("SELECT * FROM tblfotos WHERE intEmpresa=%s",$e['intEmpresa']);
			$if=$mysqli->query($query);
			if($if->num_rows>0){ ?>
			<ul id="profile-media" class="media">
				<?php
				$f=$if->fetch_assoc();
				do{
					if($f['intTipo']==0){
					$img="/ff/m/".$f['txtHash'].".jpg";
					}else if($f['intTipo']==1){
					$img="http://img.youtube.com/vi/".$f['txtHash']."/default.jpg";
					}
					?>
				<li data-hash="<?php echo $f['txtHash']; ?>" data-tipo="<?php echo $f['intTipo']; ?>" style="background-image:url('<?php echo $img; ?>');"></li>
				<?php }while($f=$if->fetch_assoc()); ?>
			</ul>
			<?php } ?>
			<?php
			$markers=array();
			$marker=array($e['dblLat'],$e['dblLong']);
			array_push($markers, $marker);
			?>
			<?php if($e['dblLat']!=0&&$e['dblLong']!=0){ ?>
			<div style="text-align:center;" id="map" data-markers='<?php echo json_encode($markers); ?>'></div>
			
			<div class="tac" style="width:100%;">
			<button id="ruta" class="btn">¿Cómo llegar?</button>
			</div>
			<div id="rutacont" class="tac" style="border:3px solid #036;width: 95%;margin: 0 auto; background:#e5c9f4; padding:5px; line-height:20px; padding:10px;display:none;">
				

				<div id="from"></div>
				<div>
				<span id="duration"></span>
				<span id="total"></span>
				</div>
			</div>
			<?php } ?>
			<ul class="social">
				<?php if($e['FBID']){ ?>
				<li><div class="tac" style="background:#036;color:#FFF;padding:5px;font-size:18px;"><i class="kon-facebook"></i></div><div class="fb-page" data-href="https://www.facebook.com/<?php echo $e['FBID']; ?>" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div></li>
				<?php } ?>
				<?php if($e['TWID']!=""){ ?>
				<li><div class="tac" style="background:#1da1f2;color:#FFF;padding:5px;font-size:18px;"><i class="kon-twitter"></i></div><a class="twitter-timeline" href="https://twitter.com/<?php echo $e['TWID']; ?>"></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div id="overlay" style="display:none;position:fixed;top:0;left:0;width:100%;height:100%;background-color:black;opacity:0.5;z-index:1000;"></div>
	<style type="text/css">
	#map{
		width: 95%;
		height: 250px;
		margin: 0 auto;
		border: 3px solid #036;
	}
	#dialog-media img{
		max-width: 100%;
		max-height: 100%;
		text-align: center;
		display: inline-block;
	}
	</style>
	<div id="dialog-media" style="text-align:center;display:none;position:fixed;top:0;left:0;width:90%;height:90%;margin: 0 5%;background-color:black;z-index:1000;"></div>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAl4aQuUYYqyIHtkOOYOHwV3LKcJBsIDrM&callback=initMap"
        async defer></script>
</body>
</html>